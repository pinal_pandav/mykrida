package com.mykrida.android.API;

import com.mykrida.android.Model.AtheleData.AtheleResponse;
import com.mykrida.android.Model.AtheleDetails.AtheleDetailsResponse;
import com.mykrida.android.Model.BookAthele.BookAtheleResponse;
import com.mykrida.android.Model.CommonResponse;
import com.mykrida.android.Model.tournaments.TournamentsResponse;
import com.mykrida.android.Model.wallet_amount.UpdateWalletAmountResponse;
import com.mykrida.android.Model.wallet_amount.WalletAmountResponse;

import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {

    @Multipart
    @POST("sendOtp")
    Call<CommonResponse> SendOTP(@Part("app_key") String key,
                                 @Part("ph_no") String mobileNo);

    @Multipart
    @POST("resendOTP")
    Call<CommonResponse> resendOTP(@Part("app_key") String key,
                                 @Part("ph_no") String mobileNo);

    @Multipart
    @POST("VerifyOtp")
    Call<String> VerifyOTP(@Part("app_key") String key,
                           @Part("ph_no") String mobileNo,
                           @Part("otp") String OTP);

    @Multipart
    @POST("cat/list")
    Call<String> GetCategoryList(@Part("app_key") String key);

    @Multipart
    @POST("search/turf")
    Call<String> GetTrufList(@Part("app_key") String key,
                             @Part("catId") int cat_ID,
                             @Part("date") String date,
                             @Part("city") String city);

    @Multipart
    @POST("turf/details/{id}")
    Call<String> GetTrufDetails(@Part("app_key") String key,
                                @Path("id") int trufID);

    @Multipart
    @POST("AvlSlot/{truf_id}/{date}")
    Call<String> GetAvailableSlots(@Part("app_key") String key,
                                   @Path("truf_id") int trufID,
                                   @Path("date") String date);

    @Multipart
    @POST("turf/booking")
    Call<String> BookTruf(@Part("app_key") String key,
                          @Part("tid") int trufID,
                          @Part("time_id") int timeID,
                          @Part("date") String date,
                          @Part("price") int Price,
                          @Part("uid") int uid,
                          @Part("trans_id") String trans_id,
                          @Part("payment_status") String payment_status,
                          @Part("booking_name") String booking_name,
                          @Part("address") String address,
                          @Part("city") String city,
                          @Part("state") String state,
                          @Part("pincode") String pincode,
                          @Part("coupon") String coupon);

    @Multipart
    @POST("users/bookings/{user_id}")
    Call<String> GetBookingDetails(@Part("app_key") String key,
                                   @Path("user_id") int user_id);

    @Multipart
    @POST("coupon/list")
    Call<String> GetCoupons(@Part("app_key") String key);

    @Multipart
    @POST("city/list")
    Call<String> GetCity(@Part("app_key") String key);

    @Multipart
    @POST("check/coupon/{code}")
    Call<String> VerifyCoupon(@Part("app_key") String key,
                              @Path("code") String code);

    @Multipart
    @POST("add/review")
    Call<String> AddReview(@Part("app_key") String key,
                           @Part("uid") int uid,
                           @Part("turf_id") int truf_id,
                           @Part("review") String review);

    @Multipart
    @POST("add/rate")
    Call<String> AddRate(@Part("app_key") String key,
                         @Part("uid") int uid,
                         @Part("turf_id") int truf_id,
                         @Part("rate") float rate);


    @Multipart
    @POST("turf/review/list/{id}")
    Call<String> GetReviewList(@Part("app_key") String key,
                               @Path("id") int truf_id);

    @Multipart
    @POST("update/user")
    Call<String> UpdateProfile(@Part("app_key") String key,
                               @Part("name") String name,
                               @Part("email") String email,
                               @Part("city") String city,
                               @Part("phone_no") String phoneno,
                               @Part("address") String address,
                               @Part("set_city") String setCity,
                               @Part("uid") int uid);

    @Multipart
    @POST("get/user/detail/{id}")
    Call<String> GetProfile(@Part("app_key") String key,
                            @Path("id") int id);

    @Multipart
    @POST("arroundCity/{cityname}")
    Call<String> GetAroundCity(@Part("app_key") String key,
                               @Path("cityname") String cityName);

    @Multipart
    @POST("bulkBooking")
    Call<String> BulkBooking(@Part("app_key") String key,
                             @Part("cid") int cid,
                             @Part("turf_id") String trufid,
                             @Part("from_time") String fromTime,
                             @Part("to_time") String toTime,
                             @Part("from_date") String fromDate,
                             @Part("to_date") String toDate);

    @Multipart
    @POST("turf/turflist")
    Call<String> TurfList(@Part("app_key") String key);

    @Multipart
    @POST("slider")
    Call<String> GetSlider(@Part("app_key") String key);

    @Multipart
    @POST("tournament/list")
    Call<String> GetTournamentList(@Part("app_key") String key);

    @Multipart
    @POST("event/list")
    Call<String> GetEventList(@Part("app_key") String key);

    @Multipart
    @POST("event/booking")
    Call<String> EventBooking(@Part("app_key") String key,
                              @Part("uid") int uid,
                              @Part("eid") int eid,
                              @Part("no_of_ticket") String no_of_ticket,
                              @Part("tot_amount") String totalAmount);

    @Multipart
    @POST("athelete-list")
    Call<AtheleResponse> getAtheleList(@Part("app_key") String key);

    @Multipart
    @POST("users/eventsbooking/{user_id}/tournaments")
    Call<TournamentsResponse> getTournaments(@Part("app_key") String key,
                                             @Path("user_id") int user_id);


    @Multipart
    @POST("athelete-details")
    Call<AtheleDetailsResponse> getAtheletsDetails(@Part("app_key") String key,
                                                   @Part("athelete_id") String athelesId);


    @Multipart
    @POST("myevent/{user_id}")
    Call<String> GetMyEventsList(@Part("app_key") String key,
                                 @Path("user_id") int user_id);

    @Multipart
    @POST("athelete-inquery")
    Call<BookAtheleResponse> AtheleteInquery(@Part("app_key") String key,
                                             @Part("atheletes_id") String atheletes_id,
                                             @Part("name") String name,
                                             @Part("email") String email,
                                             @Part("mobile_no") String mobile_no,
                                             @Part("adress") String adress,
                                             @Part("requirement") String requirement);

    @Multipart
    @POST("getwalletamount/{user_id}")
    Call<WalletAmountResponse> getWalletAmount(@Part("app_key") String key,
                                               @Path("user_id") int user_id);

    @Multipart
    @POST("contact_inquery")
    Call<BookAtheleResponse> contactUs(@Part("app_key") String key,
                                         @Part("name") String name,
                                         @Part("email") String email,
                                         @Part("subject") String subject,
                                         @Part("message") String message);

    @Multipart
    @POST("cancelBooking")
    Call<BookAtheleResponse> cancelBooking(@Part("app_key") String key,
                                       @Part("booking_id") int bookingId,
                                       @Part("user_id") int userId,
                                           @Part("refund") int refund);


    @Multipart
    @POST("updatewalletamount")
    Call<UpdateWalletAmountResponse> updateWalletAmount(@Part("app_key") String key,
                                                        @Part("user_id") int userid,
                                                        @Part("amount") String amount);


}