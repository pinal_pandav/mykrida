package com.mykrida.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.databinding.ActivityAboutUsBinding;

public class AboutUsActivity extends Activity {

    ActivityAboutUsBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us);

        binding.btnBack.setOnClickListener(view -> onBackPressed());

    }


}