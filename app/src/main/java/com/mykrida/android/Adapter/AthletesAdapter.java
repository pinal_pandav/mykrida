package com.mykrida.android.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.mykrida.android.Model.AtheleData.Athele;
import com.mykrida.android.Model.BookingDetails;
import com.mykrida.android.R;
import com.mykrida.android.databinding.ItemAthletesListBinding;

import java.util.ArrayList;

public class AthletesAdapter extends RecyclerView.Adapter<AthletesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Athele> data_list;
    private ClickListener clickListener;

    public AthletesAdapter(Context context, ArrayList<Athele> data_list) {
        this.context = context;
        this.data_list = data_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ItemAthletesListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_athletes_list, parent, false);
        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Athele athele = data_list.get(position);
        holder.itemView.tvKheladiName.setText(athele.getName());
        holder.itemView.tvSport.setText(athele.getSports());
        holder.itemView.tvTime.setText(athele.getTime() + " " + athele.getTime_hours());
        holder.itemView.tvCountry.setText(athele.getLocation());
        holder.itemView.tvCity.setText(athele.getArea());
        holder.itemView.tvDescription.setText(athele.getBio());
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(athele.getProfile_pic())
                .apply(options)
                .into(holder.itemView.ivProfile);

       // Glide.with(context).load(athele.getProfile_pic()).apply(new RequestOptions().override(200, 200)).placeholder(R.drawable.default_image).centerCrop().into(holder.itemView.ivProfile);

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private final ItemAthletesListBinding itemView;


        public ViewHolder(ItemAthletesListBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
            itemView.getRoot().setOnClickListener(this);
            itemView.getRoot().setOnLongClickListener(this);
            /*llSlotsAvailable = itemView.findViewById(R.id.llSlotsAvailable);
            tvTrufName = itemView.findViewById(R.id.tvTrufName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvRating = itemView.findViewById(R.id.tvRating);
            imgTruf = itemView.findViewById(R.id.imgTruf);
            tvBookingName = itemView.findViewById(R.id.tvBookingName);*/

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}