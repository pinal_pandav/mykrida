package com.mykrida.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mykrida.android.Model.AvailableSlots;
import com.mykrida.android.R;

import java.util.ArrayList;
import java.util.logging.Handler;

public class AvailableSlotsAdapter extends RecyclerView.Adapter<AvailableSlotsAdapter.ViewHolder> {

    private Context context;
    Activity activity;
    private ArrayList<AvailableSlots> data_list;
    String categories_title;
    private ClickListener clickListener;
    public int lastCheckedPosition = 0;


    public AvailableSlotsAdapter(Context context, String str, ArrayList<AvailableSlots> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
        this.activity = (Activity) context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_available_slots, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AvailableSlots availableSlots = data_list.get(position);
        holder.tvTime.setText(availableSlots.getFrom_time() + " - " + availableSlots.getTo_time());
        holder.tvPrice.setText("\u20B9 " +String.valueOf(availableSlots.getPrice()));
        holder.cbTime.setEnabled(false);


        if(lastCheckedPosition == position){
            availableSlots.setChecked(true);
            data_list.set(position,availableSlots);
            holder.cbTime.setImageResource(R.drawable.checkbox_filled);
        }else{
            availableSlots.setChecked(false);
            data_list.set(position,availableSlots);
            holder.cbTime.setImageResource(R.drawable.checkbox_unfilled);
        }
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {

        TextView tvTime,tvPrice;
        ImageView cbTime;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tvTime = itemView.findViewById(R.id.tvTime);
            cbTime = itemView.findViewById(R.id.cbTimeSlots);
            tvPrice = itemView.findViewById(R.id.tvPrice);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v,data_list);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v,data_list);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v,ArrayList<AvailableSlots> availableSlots);
        void onItemLongClick(int position, View v,ArrayList<AvailableSlots> availableSlots);
    }
}