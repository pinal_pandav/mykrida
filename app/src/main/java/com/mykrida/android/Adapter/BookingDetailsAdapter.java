package com.mykrida.android.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mykrida.android.Model.BookingDetails;
import com.mykrida.android.R;

import java.util.ArrayList;

public class BookingDetailsAdapter extends RecyclerView.Adapter<BookingDetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<BookingDetails> data_list;
    String categories_title;
    private ClickListener clickListener;

    public BookingDetailsAdapter(Context context, String str, ArrayList<BookingDetails> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booking_details, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BookingDetails bookingDetails = data_list.get(position);
        holder.tvTrufName.setText(bookingDetails.getTname());
        holder.tvPrice.setText("\u20B9 " + String.valueOf(bookingDetails.getPrice()));
        holder.tvDate.setText(bookingDetails.getDate());
        if(String.valueOf(bookingDetails.getRate()).equals("0") || String.valueOf(bookingDetails.getRate()).contains("-")){
            holder.tvRating.setText("⭐ -- ");
        }else {
            holder.tvRating.setText("⭐ " + String.valueOf(bookingDetails.getRate()));
        }
        holder.tvBookingName.setText(bookingDetails.getBooking_name());
        holder.tvBookingStatus.setText("Status : "+bookingDetails.getBooking_status());
        if (!bookingDetails.getImages().contains(",")) {
            Glide.with(context).load(bookingDetails.getImages()).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.imgTruf.setImageResource(R.drawable.default_image);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into(holder.imgTruf);
        }

        holder.llSlotsAvailable.setVisibility(View.VISIBLE);

    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        LinearLayout llSlotsAvailable;
        TextView tvTrufName, tvPrice, tvDate, tvRating, tvBookingName,tvBookingStatus;
        ImageView imgTruf;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            llSlotsAvailable = itemView.findViewById(R.id.llSlotsAvailable);
            tvTrufName = itemView.findViewById(R.id.tvTrufName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvRating = itemView.findViewById(R.id.tvRating);
            imgTruf = itemView.findViewById(R.id.imgTruf);
            tvBookingName = itemView.findViewById(R.id.tvBookingName);
            tvBookingStatus = itemView.findViewById(R.id.tvBookingStatus);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }
}