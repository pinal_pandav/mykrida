package com.mykrida.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.R;

import java.util.ArrayList;

public class CouponsAdapter extends RecyclerView.Adapter<CouponsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CouponModel> data_list;
    public int selectedPosition = -1;
    String categories_title;
    private ClickListener clickListener;
    boolean isApply;
    Activity mActivity;

    public CouponsAdapter(Context context, String str, ArrayList<CouponModel> data_list, String categories_title, boolean isApply) {
        this.context = context;
        this.mActivity = (Activity) context;
        this.data_list = data_list;
        this.categories_title = categories_title;
        this.isApply = isApply;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coupons, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CouponModel couponModel = data_list.get(position);
        if(!couponModel.getImage().trim().isEmpty()){
            Glide.with(context).load(couponModel.getImage()).centerCrop().into(holder.imgCoupon);
        }
        if(isApply){
            holder.btnApplyCoupon.setVisibility(View.VISIBLE);
        }else{
            holder.btnApplyCoupon.setVisibility(View.GONE);
        }
        holder.tvDiscount.setText(String.valueOf(couponModel.getDiscount()) + "% OFF");
        holder.tvDiscount1.setText(String.valueOf(couponModel.getDiscount()) + "% Discount");
        holder.tvCoupon.setText(couponModel.getCoupon_code());
        holder.btnApplyCoupon.setOnClickListener(view -> {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("data",data_list.get(position));
            mActivity.setResult(Activity.RESULT_OK,returnIntent);
            mActivity.finish();
        });
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView imgCoupon;
        TextView btnApplyCoupon;
        TextView tvDiscount,tvCoupon,tvDiscount1;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            imgCoupon = itemView.findViewById(R.id.imgCoupon);
            btnApplyCoupon = itemView.findViewById(R.id.btnApplyCoupon);
            tvDiscount = itemView.findViewById(R.id.tvPercentage);
            tvCoupon = itemView.findViewById(R.id.tvCouponCode);
            tvDiscount1 = itemView.findViewById(R.id.tvDiscount);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}