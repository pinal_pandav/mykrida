package com.mykrida.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.R;

import java.util.ArrayList;

public class DiscountAndOffersAdapter extends RecyclerView.Adapter<DiscountAndOffersAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CouponModel> data_list;
    public int selectedPosition = -1;
    String categories_title;
    private CouponsAdapter.ClickListener clickListener;

    public DiscountAndOffersAdapter(Context context, String str, ArrayList<CouponModel> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dis_offers, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        CouponModel couponModel = data_list.get(position);
        if(!couponModel.getImage().trim().isEmpty()){
            Glide.with(context).load(couponModel.getImage()).centerCrop().into(holder.imgCoupon);
        }
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        ImageView imgCoupon;
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            imgCoupon = itemView.findViewById(R.id.imgCoupon);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(CouponsAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}