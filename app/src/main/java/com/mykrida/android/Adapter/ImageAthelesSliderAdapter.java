package com.mykrida.android.Adapter;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.mykrida.android.Model.AtheleDetails.Images;
import com.mykrida.android.Model.ImageSliderModel;
import com.mykrida.android.R;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ImageAthelesSliderAdapter extends SliderViewAdapter<ImageAthelesSliderAdapter.SliderAdapter> {

    private Context context;
    // list for storing urls of images.
    private List<Images> mSliderItems = new ArrayList<>();

    // Constructor
    public ImageAthelesSliderAdapter(Context context, ArrayList<Images> sliderDataArrayList) {
        this.context = context;
        this.mSliderItems = sliderDataArrayList;
    }

    public void renewItems(List<Images> sliderItems) {
        this.mSliderItems = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mSliderItems.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(Images sliderItem) {
        this.mSliderItems.add(sliderItem);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapter onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slider, null);
        return new SliderAdapter(inflate);
    }

    // Inside on bind view holder we will
    // set data to item of Slider View.
    @Override
    public void onBindViewHolder(SliderAdapter viewHolder, final int position) {

        Images sliderItem = mSliderItems.get(position);
        if(sliderItem.getImage() != null) {

            //  set image from url.
            Glide.with(viewHolder.itemView)
                    .load(sliderItem.getImage())
                    .centerCrop()
                    .into(viewHolder.imageViewBackground);
        }else{
            viewHolder.imageViewBackground.setImageResource(sliderItem.getDrawableImage());
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mSliderItems.size();
    }

    class SliderAdapter extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;

        public SliderAdapter(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
