package com.mykrida.android.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mykrida.android.Model.SearchModel;
import com.mykrida.android.Model.TrufModel;
import com.mykrida.android.R;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private Context context;
    private ArrayList<TrufModel> data_list;
    String categories_title;
    private ClickListener clickListener;

    public SearchAdapter(Context context, String str, ArrayList<TrufModel> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        TrufModel trufModel = data_list.get(position);
        holder.tvTrufName.setText(trufModel.getTname());
        holder.tvPrice.append(" "+String.valueOf(trufModel.getPrice()));
        holder.tvPricingSet.setText(trufModel.getProcingset());
        if(String.valueOf(trufModel.getRating()).equals("0")){
            holder.tvRating.setText("⭐ -- ");
        }else {
            holder.tvRating.setText("⭐ " + String.valueOf(trufModel.getRating()));
        }
        if (!trufModel.getImages().contains(",")) {
            Glide.with(context).load(trufModel.getImages()).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    holder.imgTruf.setImageResource(R.drawable.default_image);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into(holder.imgTruf);
        }

//        if((position%2) == 0){
            holder.llBookOffline.setVisibility(View.GONE);
            holder.llSlotsAvailable.setVisibility(View.VISIBLE);
//        }else{
//            holder.llBookOffline.setVisibility(View.VISIBLE);
//            holder.llSlotsAvailable.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {

        LinearLayout llSlotsAvailable,llBookOffline;
        TextView tvTrufName,tvPrice,tvPricingSet,tvRating;
        ImageView imgTruf;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            llSlotsAvailable = itemView.findViewById(R.id.llSlotsAvailable);
            llBookOffline = itemView.findViewById(R.id.llBookOffline);
            tvTrufName = itemView.findViewById(R.id.tvTrufName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvPricingSet = itemView.findViewById(R.id.tvPricingSet);
            tvRating = itemView.findViewById(R.id.tvRating);
            imgTruf = itemView.findViewById(R.id.imgTruf);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}