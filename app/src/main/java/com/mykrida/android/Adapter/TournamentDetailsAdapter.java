package com.mykrida.android.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.mykrida.android.Model.EventDetailsModel;
import com.mykrida.android.Model.tournaments.Tournaments;
import com.mykrida.android.R;

import java.util.ArrayList;

public class TournamentDetailsAdapter extends RecyclerView.Adapter<TournamentDetailsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Tournaments> data_list;
    String categories_title;
    private ClickListener clickListener;

    public TournamentDetailsAdapter(Context context, String str, ArrayList<Tournaments> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_events_details, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Tournaments eventDetailsModel = data_list.get(position);
        holder.tvEventName.setText(eventDetailsModel.getTitle());
        holder.tvPlace.setText("Place : " + eventDetailsModel.getPlace());
        holder.tvBookingDate.setText("Booking Date : "+eventDetailsModel.getBooking_date());
        holder.tvAmount.setText("\u20B9" + eventDetailsModel.getTotal_amount());


        /*Glide.with(context).load(eventDetailsModel.getImages()).addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                holder.imgEvent.setImageResource(R.drawable.default_image);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                return false;
            }
        }).into(holder.imgEvent);*/
    }


    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {


        TextView tvEventName, tvBookingDate, tvAmount, tvPlace;
        ImageView imgEvent;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tvEventName = itemView.findViewById(R.id.tvEventName);
            tvBookingDate = itemView.findViewById(R.id.tvBookingDate);
            tvAmount = itemView.findViewById(R.id.tvAmount);
            tvPlace = itemView.findViewById(R.id.tvPlace);
            imgEvent = itemView.findViewById(R.id.imgEvent);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

        void onItemLongClick(int position, View v);
    }
}