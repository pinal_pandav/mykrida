package com.mykrida.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mykrida.android.Model.TournamentModel;
import com.mykrida.android.R;

import java.util.ArrayList;

public class UpcomingTournamentsAdapter extends RecyclerView.Adapter<UpcomingTournamentsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<TournamentModel> data_list;
    public int selectedPosition = -1;
    String categories_title;
    private UpcomingTournamentsAdapter.ClickListener clickListener;

    public UpcomingTournamentsAdapter(Context context, String str, ArrayList<TournamentModel> data_list, String categories_title) {
        this.context = context;
        this.data_list = data_list;
        this.categories_title = categories_title;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_upcoming_tournaments, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        TournamentModel tournamentModel = data_list.get(position);
        holder.tvTitle.setText(tournamentModel.getTitle());
        Glide.with(context)
                .load(tournamentModel.getImage())
                .centerCrop()
                .into(holder.imgTournament);
    }

    @Override
    public int getItemCount() {
        return data_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView tvTitle;
        ImageView imgTournament;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            imgTournament = itemView.findViewById(R.id.imgTournament);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }

        @Override
        public boolean onLongClick(View v) {
            clickListener.onItemLongClick(getAdapterPosition(), v);
            return false;
        }
    }

    public void setOnItemClickListener(UpcomingTournamentsAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
        void onItemLongClick(int position, View v);
    }
}