package com.mykrida.android;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.ImageAthelesSliderAdapter;
import com.mykrida.android.Model.AtheleDetails.AtheleDetails;
import com.mykrida.android.Model.AtheleDetails.AtheleDetailsResponse;
import com.mykrida.android.Model.AtheleDetails.Images;
import com.mykrida.android.databinding.ActivityAtheleDetailsBinding;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AtheletsDetailsActivity extends AppCompatActivity {

    ImageView btnBack, imgTruf;
    //TextView tvDescription,tvTrufName,tvBookingDate,tvPrice,tvTransactionID,tvPersonName,tvAddress,tvCity,tvState,tvPincode;
    SmoothBottomBar smoothBottomBar;
    CardView btnBookNow;
    ActivityAtheleDetailsBinding binding;
    ApiInterface apiService;
    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_athele_details);

        getSupportActionBar().hide();

        hud = KProgressHUD.create(AtheletsDetailsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        //BookingDetails bookingDetails = getIntent().getExtras().getParcelable("data");

        findViews();

        /*tvDescription.setText(bookingDetails.getDescription());
        tvTrufName.setText(bookingDetails.getTname());
        tvTransactionID.setText(bookingDetails.getTrans_id());
        tvState.setText(bookingDetails.getState());
        tvPrice.setText("\u20B9 " + String.valueOf(bookingDetails.getPrice()));
        tvPincode.setText(String.valueOf(bookingDetails.getPincode()));
        tvPersonName.setText(bookingDetails.getBooking_name());
        tvCity.setText(bookingDetails.getCity());
        tvBookingDate.setText(bookingDetails.getDate());
        tvAddress.setText(bookingDetails.getAddress());*/


        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(AtheletsDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(AtheletsDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnBookNow.setOnClickListener(v -> {
            Intent intent = new Intent(AtheletsDetailsActivity.this, BookAtheletsActivity.class);
            int id = getIntent().getIntExtra("id", 0);
            intent.putExtra("id", id);
            startActivity(intent);

        });


        GetBookingDetails();

    }


    private void findViews() {
        btnBack = findViewById(R.id.btnBack);
        imgTruf = findViewById(R.id.img_truf);
        btnBookNow = findViewById(R.id.btnBookNow);
        /*tvDescription = findViewById(R.id.tvDescription);
        tvTrufName = findViewById(R.id.tvTrufName);
        tvBookingDate = findViewById(R.id.tvBookingDate);
        tvPrice = findViewById(R.id.tvPrice);
        tvTransactionID = findViewById(R.id.tvTransactionID);
        tvPersonName = findViewById(R.id.tvPersonName);
        tvAddress = findViewById(R.id.tvAddress);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        tvPincode = findViewById(R.id.tvPincode);*/
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        int id = getIntent().getIntExtra("id", 0);
        Call<AtheleDetailsResponse> callCard = apiService.getAtheletsDetails(Constant.APPKEY, String.valueOf(id));
        callCard.enqueue(new Callback<AtheleDetailsResponse>() {
            @Override
            public void onResponse(Call<AtheleDetailsResponse> call, Response<AtheleDetailsResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {

                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            AtheleDetails atheleDetails = response.body().getData().get(0);
                            binding.tvName.setText(atheleDetails.getName());
                            binding.tvDescription.setText(atheleDetails.getBio());
                            binding.tvArea.setText(atheleDetails.getArea());
                            binding.tvBirthday.setText(atheleDetails.getDob());
                            binding.tvLanguage.setText(atheleDetails.getLanguages());
                            binding.tvEventPrefered.setText(atheleDetails.getEvent_prefered());
                            binding.tvSports.setText(atheleDetails.getSports());
                            binding.tvLocation.setText(atheleDetails.getLocation());

                            if (response.body().getGalleryImages() != null) {
                                if (response.body().getGalleryImages().size() > 0) {
                                    ArrayList<Images> arrImage = new ArrayList<>(response.body().getGalleryImages());
                                    ImageAthelesSliderAdapter imageSliderAdapter = new ImageAthelesSliderAdapter(AtheletsDetailsActivity.this, arrImage);
                                    binding.imageSliderTop.setSliderAdapter(imageSliderAdapter);
                                    binding.imageSliderTop.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                                    binding.imageSliderTop.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                                    binding.imageSliderTop.setScrollTimeInSec(2); //set scroll delay in seconds :
                                    binding.imageSliderTop.startAutoCycle();
                                } else {
                                    ArrayList<Images> arrImage = new ArrayList<>();
                                    Images images = new Images();
                                    images.setDrawableImage(R.drawable.default_slider);
                                    arrImage.add(images);
                                    ImageAthelesSliderAdapter imageSliderAdapter = new ImageAthelesSliderAdapter(AtheletsDetailsActivity.this, arrImage);
                                    binding.imageSliderTop.setSliderAdapter(imageSliderAdapter);
                                    binding.imageSliderTop.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                                    binding.imageSliderTop.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                                    binding.imageSliderTop.setScrollTimeInSec(2); //set scroll delay in seconds :
                                    binding.imageSliderTop.startAutoCycle();
                                }
                            }else{
                                ArrayList<Images> arrImage = new ArrayList<>();
                                Images images = new Images();
                                images.setDrawableImage(R.drawable.default_slider);
                                arrImage.add(images);
                                ImageAthelesSliderAdapter imageSliderAdapter = new ImageAthelesSliderAdapter(AtheletsDetailsActivity.this, arrImage);
                                binding.imageSliderTop.setSliderAdapter(imageSliderAdapter);
                                binding.imageSliderTop.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                                binding.imageSliderTop.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                                binding.imageSliderTop.setScrollTimeInSec(2); //set scroll delay in seconds :
                                binding.imageSliderTop.startAutoCycle();
                            }
                        } else {
                            Toast.makeText(AtheletsDetailsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AtheletsDetailsActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(AtheletsDetailsActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AtheleDetailsResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }
}