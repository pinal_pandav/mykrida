package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.AthletesAdapter;
import com.mykrida.android.Model.AtheleData.Athele;
import com.mykrida.android.Model.AtheleData.AtheleResponse;
import com.mykrida.android.Model.BookingDetails;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AthletesListActivity extends AppCompatActivity {

    RecyclerView rvAthletes;
    AthletesAdapter athletesAdapter;
    ImageView btnBack;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<Athele> bookingDetailsArrayList = new ArrayList<>();
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athletes_list);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews();

        hud = KProgressHUD.create(AthletesListActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvAthletes.setHasFixedSize(true);
        rvAthletes.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvAthletes.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(AthletesListActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(AthletesListActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        GetBookingDetails();

    }


    private void findViews() {
        rvAthletes = findViewById(R.id.rvAthletes);
        btnBack = findViewById(R.id.btnBack);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<AtheleResponse> callCard = apiService.getAtheleList(Constant.APPKEY);
        callCard.enqueue(new Callback<AtheleResponse>() {
            @Override
            public void onResponse(Call<AtheleResponse> call, Response<AtheleResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {

                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            bookingDetailsArrayList = response.body().getData();
                            if (bookingDetailsArrayList.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                athletesAdapter = new AthletesAdapter(AthletesListActivity.this, bookingDetailsArrayList);
                                rvAthletes.setAdapter(athletesAdapter);
                                setAdapterClick();
                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }
                        }else{
                            Toast.makeText(AthletesListActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(AthletesListActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(AthletesListActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                }
        }

        @Override
        public void onFailure (Call < AtheleResponse > call, Throwable t){
            hud.dismiss();
        }
    });
}

    private void setAdapterClick() {

        athletesAdapter.setOnItemClickListener(new AthletesAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(AthletesListActivity.this, AtheletsDetailsActivity.class);
                intent.putExtra("id",bookingDetailsArrayList.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });


    }


}