package com.mykrida.android;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.AthletesAdapter;
import com.mykrida.android.Model.AtheleData.Athele;
import com.mykrida.android.Model.AtheleData.AtheleResponse;
import com.mykrida.android.Model.BookingDetails;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AthletesListFragment extends Fragment {

    RecyclerView rvAthletes;
    AthletesAdapter athletesAdapter;
    ImageView btnBack;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<Athele> bookingDetailsArrayList = new ArrayList<>();
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    TextView tvNoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_athletes_list, container, false);

        prefUserData = getActivity().getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews(v);

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvAthletes.setHasFixedSize(true);
        rvAthletes.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvAthletes.setLayoutManager(llmF);


        GetBookingDetails();

        return v;

    }


    private void findViews(View v) {
        rvAthletes = v.findViewById(R.id.rvAthletes);
        btnBack = v.findViewById(R.id.btnBack);
        tvNoDataFound = v.findViewById(R.id.tvNoDataFound);
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<AtheleResponse> callCard = apiService.getAtheleList(Constant.APPKEY);
        callCard.enqueue(new Callback<AtheleResponse>() {
            @Override
            public void onResponse(Call<AtheleResponse> call, Response<AtheleResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {

                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            bookingDetailsArrayList = response.body().getData();
                            if (bookingDetailsArrayList.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                athletesAdapter = new AthletesAdapter(getActivity(), bookingDetailsArrayList);
                                rvAthletes.setAdapter(athletesAdapter);
                                setAdapterClick();
                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }
                        }else{
                            Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                }
        }

        @Override
        public void onFailure (Call < AtheleResponse > call, Throwable t){
            hud.dismiss();
        }
    });
}

    private void setAdapterClick() {

        athletesAdapter.setOnItemClickListener(new AthletesAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(getActivity(), AtheletsDetailsActivity.class);
                intent.putExtra("id",bookingDetailsArrayList.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });


    }


}