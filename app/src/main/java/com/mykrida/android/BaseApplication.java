package com.mykrida.android;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDex;


/**
 * Created by Rahul Hooda on 14/7/17.
 */
public class BaseApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

    }

}
