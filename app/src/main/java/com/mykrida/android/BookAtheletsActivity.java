package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.AvailableSlots;
import com.mykrida.android.Model.BookAthele.BookAtheleResponse;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.Model.TrufModel;
import com.mykrida.android.databinding.ActivityAtheleDetailsBinding;
import com.mykrida.android.databinding.ActivityBookAtheleBinding;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookAtheletsActivity extends Activity {

    ImageView btnBack;
    CardView btnSend;

    ApiInterface apiService;
    KProgressHUD hud;

    SharedPreferences prefUserData;
    JsonObject jobjUser;
    SmoothBottomBar smoothBottomBar;

    ActivityBookAtheleBinding binding;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_book_athele);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(BookAtheletsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews();


        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Validate()) {
                    BookAthele(binding.edtName.getText().toString().trim(),
                            binding.edtEmail.getText().toString().trim(),
                            binding.edtMobileNumber.getText().toString().trim(),
                            binding.edtAddress.getText().toString().trim(),
                            binding.edtDetails.getText().toString().trim());
                }
            }
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(BookAtheletsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(BookAtheletsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });



    }

    private boolean Validate() {
        String name = binding.edtName.getText().toString().trim();
        String email = binding.edtEmail.getText().toString().trim();
        String mobileno = binding.edtMobileNumber.getText().toString().trim();
        String address = binding.edtAddress.getText().toString().trim();
        String details = binding.edtDetails.getText().toString().trim();
        if (name.isEmpty()) {
            binding.edtName.setError("Please enter Name");
            return false;
        } else if (email.isEmpty()) {
            binding.edtEmail.setError("Please enter Email");
            return false;
        } else if (!email.matches(emailPattern)) {
            binding.edtEmail.setError("Please enter valid Email");
            return false;
        } else if (mobileno.isEmpty()) {
            binding.edtMobileNumber.setError("Please enter mobile number");
            return false;
        } else if (mobileno.length() != 10) {
            binding.edtMobileNumber.setError("Please enter valid mobile number");
            return false;
        } else if (address.isEmpty()) {
            binding.edtAddress.setError("Please enter Address");
            return false;
        } else if (details.isEmpty()) {
            binding.edtDetails.setError("Please enter Details");
            return false;
        } else {
            return true;
        }
    }


    private void findViews() {
        btnSend = findViewById(R.id.btnSend);
    }

    private void BookAthele(String name, String email, String mobileno, String address, String requirement) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        int id  = getIntent().getIntExtra("id",0);

        Call<BookAtheleResponse> callCard = apiService.AtheleteInquery(Constant.APPKEY,
                String.valueOf(id),
                name,email,
                mobileno,
                address,
                requirement);
        callCard.enqueue(new Callback<BookAtheleResponse>() {
            @Override
            public void onResponse(Call<BookAtheleResponse> call, Response<BookAtheleResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                        assert response.body() != null;
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(BookAtheletsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(BookAtheletsActivity.this, DashboardActivity.class);
                            intent.putExtra("position", 0);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                }
            }

            @Override
            public void onFailure(Call<BookAtheleResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

}