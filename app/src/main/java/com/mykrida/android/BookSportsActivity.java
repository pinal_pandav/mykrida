package com.mykrida.android;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.AvailableSlotsAdapter;
import com.mykrida.android.Model.AvailableSlots;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.Model.TrufModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookSportsActivity extends Activity {

    RecyclerView rvAvailableSlots;
    AvailableSlotsAdapter availableSlotsAdapter;
    CardView btnCheckout;
    ImageView btnBack;
    Calendar myCalendar = Calendar.getInstance();
    TextView tvDate;
    String strSelectedDate;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<AvailableSlots> availableSlotsArrayList = new ArrayList<>();
    int Price = 0,trufID;
    TextView tvPrice,tvTrufName;
    TrufModel trufModel;
    LinearLayout llNoDataFound;
    SmoothBottomBar smoothBottomBar;
    TextView btnViewCoupons;
    CardView btnApplyCoupon;
    EditText edtCouponCode;
    TextView tvBtnText;
    CouponModel couponModel = null;
    CircleImageView imgTruf;
    String Date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_sports);


        findViews();

        Price = Integer.parseInt(getIntent().getStringExtra("price"));
        trufID = getIntent().getIntExtra("trufID",0);
        trufModel = getIntent().getExtras().getParcelable("data");
        Date = getIntent().getStringExtra("Date");

        tvTrufName.setText(trufModel.getTname());

        Glide.with(this).load(trufModel.getImages()).centerCrop().into(imgTruf);

        hud = KProgressHUD.create(BookSportsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        rvAvailableSlots.setHasFixedSize(true);
        rvAvailableSlots.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvAvailableSlots.setLayoutManager(llmF);


        btnCheckout.setOnClickListener(view -> {

            if(!edtCouponCode.getText().toString().trim().isEmpty()){
                if(!isApplied) {
                    Toast.makeText(this, "Please Applied the Coupon First", Toast.LENGTH_SHORT).show();
                }else{

                    if(isSlotSelected) {
                        Intent i = new Intent(BookSportsActivity.this, PaymentActivity.class);
                        i.putExtra("data", availableSlotsArrayList.get(availableSlotsAdapter.lastCheckedPosition));
                        i.putExtra("trufdata", trufModel);
                        i.putExtra("date", strSelectedDate);
                        if (couponModel != null) {
                            i.putExtra("coupon", couponModel);
                        }
                        startActivity(i);
                    }else{
                        Toast.makeText(this, "Please select slot", Toast.LENGTH_SHORT).show();
                    }
                }
            }else {
                if(isSlotSelected){
                    Intent i = new Intent(BookSportsActivity.this, PaymentActivity.class);
                    i.putExtra("data", availableSlotsArrayList.get(availableSlotsAdapter.lastCheckedPosition));
                    i.putExtra("trufdata", trufModel);
                    i.putExtra("date", strSelectedDate);
                    if (couponModel != null) {
                        i.putExtra("coupon", couponModel);
                    }
                    startActivity(i);
                }else{
                    Toast.makeText(this, "Please select slot", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        tvDate.setOnClickListener(view -> {

            DatePickerDialog datePicker = new DatePickerDialog(BookSportsActivity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();


        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(BookSportsActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(BookSportsActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnViewCoupons.setOnClickListener(view -> {
            Intent intent = new Intent(BookSportsActivity.this,CouponsActivity.class);
            intent.putExtra("Apply",true);
            startActivityForResult(intent,100);
        });

        btnApplyCoupon.setOnClickListener(view -> {
            if(edtCouponCode.getText().toString().trim().isEmpty()){
                Toast.makeText(this, "Please enter Coupon Code", Toast.LENGTH_SHORT).show();
            }else {
                ApplyCoupon();
            }
        });

        try {
            updateLabelFirst();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    boolean isApplied = false;
    boolean isAPICheck = false;

    private void ApplyCoupon() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.VerifyCoupon(Constant.APPKEY, edtCouponCode.getText().toString().trim());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            tvBtnText.setText("Applied");
                            isApplied = true;
                        }else{
                            isApplied = true;
                            couponModel = null;
                            Toast.makeText(BookSportsActivity.this, "Coupon Code is Invalid", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    isApplied = true;
                    couponModel = null;
                    Toast.makeText(BookSportsActivity.this, "Coupon Code is Invalid", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDate.setText(sdf.format(myCalendar.getTime()));

        String myFormatforAPI = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
        strSelectedDate = sdfforAPI.format(myCalendar.getTime());

        GetAvailableTimeSlots();
    }

    private void updateLabelFirst() throws ParseException {
        strSelectedDate = Date;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date newDate = format.parse(Date);

        format = new SimpleDateFormat("dd MMM yyyy");
        String date = format.format(newDate);
        tvDate.setText(date);

        GetAvailableTimeSlots();
    }

    private void GetAvailableTimeSlots() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetAvailableSlots(Constant.APPKEY, trufID,strSelectedDate);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        availableSlotsArrayList.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                AvailableSlots availableSlots = new AvailableSlots();
                                availableSlots.setFrom_time(parseDateToddMMyyyy(jsonObject1.getString("from_time")));
                                availableSlots.setTo_time(parseDateToddMMyyyy(jsonObject1.getString("to_time")));
                                availableSlots.setTruf_id(jsonObject1.getInt("turf_id"));
                                availableSlots.setTime_id(jsonObject1.getInt("time_id"));
                                availableSlots.setPrice(jsonObject1.getInt("price"));
                                availableSlots.setChecked(false);
                                availableSlotsArrayList.add(availableSlots);
                            }

                            if(availableSlotsArrayList.size() != 0) {
                                isSlotSelected = true;
                                llNoDataFound.setVisibility(View.GONE);
                                rvAvailableSlots.setVisibility(View.VISIBLE);
                                tvPrice.setText("\u20B9 " + String.valueOf(availableSlotsArrayList.get(0).getPrice()));
                                availableSlotsAdapter = new AvailableSlotsAdapter(BookSportsActivity.this, "Fragment", availableSlotsArrayList, "");
                                rvAvailableSlots.setAdapter(availableSlotsAdapter);
                                setAdapterClick();
                            }else{
                                tvPrice.setText("\u20B9 0");
                                llNoDataFound.setVisibility(View.VISIBLE);
                                rvAvailableSlots.setVisibility(View.GONE);
                                isSlotSelected = false;
                            }

                        } else {
                            Toast.makeText(BookSportsActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }else{
                    tvPrice.setText("\u20B9 0");
                    llNoDataFound.setVisibility(View.VISIBLE);
                    rvAvailableSlots.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "HH:mm";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    boolean isSlotSelected = false;
    private void setAdapterClick() {
        availableSlotsAdapter.setOnItemClickListener(new AvailableSlotsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v, ArrayList<AvailableSlots> availableSlots) {
                availableSlotsAdapter.lastCheckedPosition = position;
                availableSlotsAdapter.notifyDataSetChanged();
                tvPrice.setText("\u20B9 " + String.valueOf(availableSlots.get(availableSlotsAdapter.lastCheckedPosition).getPrice()));
                isSlotSelected = true;
            }

            @Override
            public void onItemLongClick(int position, View v, ArrayList<AvailableSlots> availableSlots) {

            }
        });
    }


    private void findViews() {
        rvAvailableSlots = findViewById(R.id.rvAvailableSlots);
        btnCheckout = findViewById(R.id.btnCheckout);
        btnBack = findViewById(R.id.btnBack);
        tvDate = findViewById(R.id.tvDate);
        tvPrice = findViewById(R.id.tvPrice);
        tvTrufName = findViewById(R.id.tvTrufName);
        llNoDataFound = findViewById(R.id.llNoDataFound);
        btnViewCoupons = findViewById(R.id.btnViewCoupons);
        btnApplyCoupon = findViewById(R.id.btnApplyCoupon);
        edtCouponCode = findViewById(R.id.edtCouponCode);
        tvBtnText = findViewById(R.id.tvbtnText);
        imgTruf = findViewById(R.id.imgTruf);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                couponModel =data.getExtras().getParcelable("data");
                edtCouponCode.setText(couponModel.getCoupon_code());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}