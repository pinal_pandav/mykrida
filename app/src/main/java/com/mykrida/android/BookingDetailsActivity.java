package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.BookAthele.BookAtheleResponse;
import com.mykrida.android.Model.BookingDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.SneakyThrows;
import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingDetailsActivity extends AppCompatActivity {

    ImageView btnBack, imgTruf;
    TextView tvSlotTime;
    TextView tvDescription, tvTrufName, tvBookingDate, tvPrice, tvTransactionID, tvPersonName, tvAddress, tvCity, tvState, tvPincode, tvBookingStatus;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;

    SharedPreferences prefUserData;
    JsonObject jobjUser;
    CardView btnCancelBooking;


    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_details);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        btnCancelBooking = findViewById(R.id.btnCancelBooking);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        BookingDetails bookingDetails = getIntent().getExtras().getParcelable("data");

        findViews();

        tvDescription.setText(bookingDetails.getDescription());
        tvTrufName.setText(bookingDetails.getTname());
        tvTransactionID.setText(bookingDetails.getTrans_id());
        tvState.setText(bookingDetails.getState());
        tvPrice.setText("\u20B9 " + String.valueOf(bookingDetails.getPrice()));
        tvPincode.setText(String.valueOf(bookingDetails.getPincode()));
        tvPersonName.setText(bookingDetails.getBooking_name());
        tvCity.setText(bookingDetails.getCity());
        tvBookingDate.setText(bookingDetails.getDate());
        tvAddress.setText(bookingDetails.getAddress());
        tvBookingStatus.setText(bookingDetails.getBooking_status());
        tvSlotTime.setText(parseDateToddMMyyyy(bookingDetails.getSlotTime()));

        if(bookingDetails.getImages() != null) {
            Glide.with(this)
                    .load(bookingDetails.getImages())
                    .centerCrop()
                    .into(imgTruf);
        }

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(BookingDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(BookingDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnCancelBooking.setOnClickListener(view -> {
            int refund = 100;
            try {
                if (returnHour(bookingDetails.getDate(), bookingDetails.getSlotTime()) < 3) {
                    refund = 40;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cancelBooking(bookingDetails.getBooking_id(), refund);
        });

        if (returnMin(bookingDetails.getDate(), bookingDetails.getSlotTime()) > 0) {
            if (!bookingDetails.getBooking_status().equals("Cancelled")) {
                btnCancelBooking.setVisibility(View.VISIBLE);
            } else {
                btnCancelBooking.setVisibility(View.GONE);
            }
        } else {
            btnCancelBooking.setVisibility(View.GONE);
        }

    }


    private void findViews() {
        btnBack = findViewById(R.id.btnBack);
        imgTruf = findViewById(R.id.img_truf);
        tvDescription = findViewById(R.id.tvDescription);
        tvTrufName = findViewById(R.id.tvTrufName);
        tvBookingDate = findViewById(R.id.tvBookingDate);
        tvPrice = findViewById(R.id.tvPrice);
        tvTransactionID = findViewById(R.id.tvTransactionID);
        tvPersonName = findViewById(R.id.tvPersonName);
        tvAddress = findViewById(R.id.tvAddress);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        tvPincode = findViewById(R.id.tvPincode);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        tvSlotTime = findViewById(R.id.tvSlotTime);
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    private void cancelBooking(int bookingID, int refund) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<BookAtheleResponse> callCard = apiService.cancelBooking(Constant.APPKEY,
                bookingID,
                jobjUser.get("id").getAsInt(),
                refund);
        callCard.enqueue(new Callback<BookAtheleResponse>() {
            @Override
            public void onResponse(Call<BookAtheleResponse> call, Response<BookAtheleResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    finish();
                    Toast.makeText(BookingDetailsActivity.this, "Booking Cancelled Successfully", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<BookAtheleResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public int returnHour(String date, String time) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = simpleDateFormat.parse(date + " " + time);

        Date date2 = Calendar.getInstance().getTime();

        long difference = date1.getTime() - date2.getTime();
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
        if (days != 0) {
            hours = hours + (days * 24);
        }
        return hours;
    }

    public int returnMin(String date, String time) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = simpleDateFormat.parse(date + " " + time);

        Date date2 = Calendar.getInstance().getTime();

        long difference = date1.getTime() - date2.getTime();
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000*60*60*24*days) - (1000*60*60*hours)) / (1000*60);
        if (days != 0) {
            hours = hours + (days * 24);
        }
        if(hours != 0){
            min = min + (hours * 60);
        }
        return min;
    }

    public long getTimeDuration(String date, String time) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date1 = simpleDateFormat.parse(date + " " + time);

        Date date2 = Calendar.getInstance().getTime();

        long difference = date1.getTime() - date2.getTime();

        return difference;
    }

}