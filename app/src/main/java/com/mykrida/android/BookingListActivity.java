package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.AvailableSlotsAdapter;
import com.mykrida.android.Adapter.BookingDetailsAdapter;
import com.mykrida.android.Adapter.ChangeLocationAdapter;
import com.mykrida.android.Model.AvailableSlots;
import com.mykrida.android.Model.BookingDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingListActivity extends AppCompatActivity {

    RecyclerView rvBookingDetails;
    BookingDetailsAdapter bookingDetailsAdapter;
    ImageView btnBack;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<BookingDetails> bookingDetailsArrayList = new ArrayList<>();
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews();

        hud = KProgressHUD.create(BookingListActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvBookingDetails.setHasFixedSize(true);
        rvBookingDetails.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvBookingDetails.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(BookingListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(BookingListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        GetBookingDetails();

    }

    @Override
    protected void onStart() {
        super.onStart();
        GetBookingDetails();
    }

    private void findViews() {
        rvBookingDetails = findViewById(R.id.rvBookingDetails);
        btnBack = findViewById(R.id.btnBack);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetBookingDetails(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        bookingDetailsArrayList.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                BookingDetails bookingDetails = new BookingDetails();
                                bookingDetails.setAddress(jsonObject1.getString("address"));
                                bookingDetails.setBooking_name(jsonObject1.getString("booking_name"));
                                bookingDetails.setCid(jsonObject1.getInt("cid"));
                                bookingDetails.setCity(jsonObject1.getString("city"));
                                bookingDetails.setCoupon(jsonObject1.getString("coupon"));
                                bookingDetails.setDate(jsonObject1.getString("date"));
                                bookingDetails.setDescription(jsonObject1.getString("descr"));
                                bookingDetails.setId(jsonObject1.getInt("id"));
                                bookingDetails.setImages(jsonObject1.getString("images"));
                                bookingDetails.setPayment_status(jsonObject1.getString("payment_status"));
                                bookingDetails.setPincode(jsonObject1.getInt("pincode"));
                                bookingDetails.setPrice(jsonObject1.getString("bookind_price"));
                                bookingDetails.setPricing_set("");
                                bookingDetails.setRate(jsonObject1.getInt("rate"));
                                bookingDetails.setScid(jsonObject1.getInt("scid"));
                                bookingDetails.setState(jsonObject1.getString("state"));
                                bookingDetails.setTime_id(jsonObject1.getInt("time_id"));
                                bookingDetails.setTname(jsonObject1.getString("tname"));
                                bookingDetails.setTrans_id(jsonObject1.getString("trans_id"));
                                bookingDetails.setTrufid(jsonObject1.getInt("turf_id"));
                                bookingDetails.setUid(jsonObject1.getInt("uid"));
                                bookingDetails.setVid(jsonObject1.getInt("vid"));
                                bookingDetails.setBooking_id(jsonObject1.getInt("booking_id"));
                                bookingDetails.setSlotTime(jsonObject1.getString("time_slot"));
                                bookingDetails.setBooking_status(jsonObject1.getString("booking_status"));
                                bookingDetailsArrayList.add(bookingDetails);
                            }

                            if(bookingDetailsArrayList.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                bookingDetailsAdapter = new BookingDetailsAdapter(BookingListActivity.this, "Fragment", bookingDetailsArrayList, "");
                                rvBookingDetails.setAdapter(bookingDetailsAdapter);
                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(BookingListActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        bookingDetailsAdapter.setOnItemClickListener(new BookingDetailsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(BookingListActivity.this,BookingDetailsActivity.class);
                intent.putExtra("data",bookingDetailsArrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });


    }


}