package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.CenterMenuAdapter;
import com.mykrida.android.Adapter.SearchAdapter;
import com.mykrida.android.Model.TrufModel;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.OnMenuItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryDetailsActivity extends AppCompatActivity {

    RecyclerView rvSearch;
    SearchAdapter searchAdapter;
    ImageView btnBack;
    RelativeLayout rlMainLayout;
    int CategoryID;
    KProgressHUD hud;
    ApiInterface apiService;
    TextView tvNoDataFound;
    ArrayList<TrufModel> trufModelArrayList = new ArrayList<>();
    TextView tvCategory;
    SmoothBottomBar smoothBottomBar;
    SharedPreferences prefUserData;
    String Location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        getSupportActionBar().hide();


        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        Location = prefUserData.getString("location", "N/A");

        findViews();

        rvSearch.setHasFixedSize(true);
        rvSearch.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvSearch.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(CategoryDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(CategoryDetailsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

    }

    private void findViews() {
        rvSearch = findViewById(R.id.rvSearch);
        btnBack = findViewById(R.id.btnBack);
        rlMainLayout = findViewById(R.id.rlMainLayout);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
        tvCategory = findViewById(R.id.tvCategory);

        Intent i = getIntent();
        CategoryID = i.getIntExtra("id", 0);
        tvCategory.setText(i.getStringExtra("catName"));

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

//        if (Location.equals("N/A")) {
//            finish();
//            Toast.makeText(this, "Please select Location from Profile", Toast.LENGTH_SHORT).show();
//        } else {
//            GetTrufData(Date, CategoryID);
//        }
    }

    private void GetTrufData(String Date, int CategoryID) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetTrufList(Constant.APPKEY, CategoryID, Date, Location);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                TrufModel trufModel = new TrufModel();
                                trufModel.setId(jsonObject1.getInt("id"));
                                trufModel.setCid(jsonObject1.getInt("cid"));
                                trufModel.setScid(jsonObject1.getInt("scid"));
                                trufModel.setVid(jsonObject1.getInt("vid"));
                                trufModel.setTname(jsonObject1.getString("tname"));
                                trufModel.setAddress(jsonObject1.getString("address"));
                                trufModel.setDescription(jsonObject1.getString("descr"));
                                trufModel.setProcingset(jsonObject1.getString("pricing_set"));
                                trufModel.setPrice(jsonObject1.getString("turf_price"));
                                trufModel.setDay(jsonObject1.getString("day"));
                                trufModel.setFromtime(jsonObject1.getString("from_time"));
                                trufModel.setTotime(jsonObject1.getString("to_time"));
                                trufModel.setImages(jsonObject1.getString("images"));
                                trufModel.setTrufID(jsonObject1.getInt("turf_id"));
                                trufModel.setRating(jsonObject1.getInt("rate"));
                                trufModelArrayList.add(trufModel);

                            }
                            if (trufModelArrayList.size() != 0) {
                                searchAdapter = new SearchAdapter(CategoryDetailsActivity.this, "Fragment", trufModelArrayList, "");
                                rvSearch.setAdapter(searchAdapter);

                                setClickEventOfListView();
                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    tvNoDataFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void setClickEventOfListView() {
        searchAdapter.setOnItemClickListener(new SearchAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent i = new Intent(CategoryDetailsActivity.this, SportsDetailActivity.class);
                i.putExtra("trufdata", trufModelArrayList.get(position));
//                i.putExtra("date", Date);
                startActivity(i);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

}