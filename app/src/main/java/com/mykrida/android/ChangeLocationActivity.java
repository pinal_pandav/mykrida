package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.ChangeLocationAdapter;
import com.mykrida.android.Adapter.CouponsAdapter;
import com.mykrida.android.Model.CouponModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeLocationActivity extends AppCompatActivity {

    RecyclerView rvChangeLocation;
    ChangeLocationAdapter changeLocationAdapter;
    ImageView btnBack;
    CardView btnProceed;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<String> arrLocation = new ArrayList<>();
    TextView tvNoDataFound;
    SharedPreferences prefUserData;
    SharedPreferences.Editor editorUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_location);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        editorUserData = prefUserData.edit();


        hud = KProgressHUD.create(ChangeLocationActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews();

        rvChangeLocation.setHasFixedSize(true);
        rvChangeLocation.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvChangeLocation.setLayoutManager(llmF);


        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        btnProceed.setOnClickListener(view -> {
            editorUserData.putString("location",arrLocation.get(changeLocationAdapter.lastCheckedPosition));
            editorUserData.apply();
            onBackPressed();
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(ChangeLocationActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(ChangeLocationActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        GetLocation();

    }


    private void findViews() {
        rvChangeLocation = findViewById(R.id.rvLocation);
        btnBack = findViewById(R.id.btnBack);
        btnProceed = findViewById(R.id.btnProceed);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }

    private void GetLocation() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetCity(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrLocation.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                arrLocation.add(jsonObject1.getString("city"));
                            }

                            if(arrLocation.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                changeLocationAdapter = new ChangeLocationAdapter(ChangeLocationActivity.this, "Fragment", arrLocation, "");
                                rvChangeLocation.setAdapter(changeLocationAdapter);
                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(ChangeLocationActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        changeLocationAdapter.setOnItemClickListener(new ChangeLocationAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });
    }
}