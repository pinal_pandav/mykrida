package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.BookAthele.BookAtheleResponse;
import com.mykrida.android.databinding.ActivityBookAtheleBinding;
import com.mykrida.android.databinding.ActivityContactUsBinding;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends Activity {

    ImageView btnBack;
    CardView btnSend;

    ApiInterface apiService;
    KProgressHUD hud;

    SharedPreferences prefUserData;
    JsonObject jobjUser;
    SmoothBottomBar smoothBottomBar;

    ActivityContactUsBinding binding;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_contact_us);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(ContactUsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews();


        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        btnSend.setOnClickListener(view -> {
            if (Validate()) {
                contactUs(binding.edtName.getText().toString().trim(),
                        binding.edtEmail.getText().toString().trim(),
                        binding.edtSubject.getText().toString().trim(),
                        binding.edtMessage.getText().toString().trim());
            }
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(ContactUsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(ContactUsActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });



    }

    private boolean Validate() {
        String name = binding.edtName.getText().toString().trim();
        String email = binding.edtEmail.getText().toString().trim();
        String subject = binding.edtSubject.getText().toString().trim();
        String message = binding.edtMessage.getText().toString().trim();
        if (name.isEmpty()) {
            binding.edtName.setError("Please enter Name");
            return false;
        } else if (email.isEmpty()) {
            binding.edtEmail.setError("Please enter Email");
            return false;
        } else if (!email.matches(emailPattern)) {
            binding.edtEmail.setError("Please enter valid Email");
            return false;
        } else if (subject.isEmpty()) {
            binding.edtSubject.setError("Please enter Subject");
            return false;
        } else if (message.isEmpty()) {
            binding.edtMessage.setError("Please enter Message");
            return false;
        } else {
            return true;
        }
    }


    private void findViews() {
        btnSend = findViewById(R.id.btnSendMessage);
    }

    private void contactUs(String name, String email, String subject, String message) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<BookAtheleResponse> callCard = apiService.contactUs(Constant.APPKEY,
                name,
                email,
                subject,
                message);
        callCard.enqueue(new Callback<BookAtheleResponse>() {
            @Override
            public void onResponse(Call<BookAtheleResponse> call, Response<BookAtheleResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                        assert response.body() != null;
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(ContactUsActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            finish();
                        }
                }
            }

            @Override
            public void onFailure(Call<BookAtheleResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

}