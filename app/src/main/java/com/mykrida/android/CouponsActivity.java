package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.BookingDetailsAdapter;
import com.mykrida.android.Adapter.ChangeLocationAdapter;
import com.mykrida.android.Adapter.CouponsAdapter;
import com.mykrida.android.Model.BookingDetails;
import com.mykrida.android.Model.CouponModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponsActivity extends AppCompatActivity {

    RecyclerView rvCoupon;
    CouponsAdapter couponsAdapter;
    ImageView btnBack;
    SmoothBottomBar smoothBottomBar;
    ArrayList<CouponModel> arrCoupons = new ArrayList<>();
    ApiInterface apiService;
    KProgressHUD hud;
    TextView tvNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);

        getSupportActionBar().hide();

        hud = KProgressHUD.create(CouponsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews();


        rvCoupon.setHasFixedSize(true);
        rvCoupon.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvCoupon.setLayoutManager(llmF);


        btnBack.setOnClickListener(view -> {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            finish();
        });


        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(CouponsActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(CouponsActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        GetCouponList();

    }


    private void findViews() {
        rvCoupon = findViewById(R.id.rvCoupon);
        btnBack = findViewById(R.id.btnBack);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
    }


    private void GetCouponList() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetCoupons(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrCoupons.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                CouponModel couponModel = new CouponModel();
                                couponModel.setCoupon_code(jsonObject1.getString("code"));
                                couponModel.setDiscount(jsonObject1.getInt("discount"));
                                couponModel.setEnd_date(jsonObject1.getString("end_date"));
                                couponModel.setId(jsonObject1.getInt("id"));
                                couponModel.setImage(jsonObject1.getString("image"));
                                couponModel.setStart_date(jsonObject1.getString("start_date"));

                                arrCoupons.add(couponModel);
                            }

                            if(arrCoupons.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                couponsAdapter = new CouponsAdapter(CouponsActivity.this, "Fragment", arrCoupons, "",getIntent().getBooleanExtra("Apply",false));
                                rvCoupon.setAdapter(couponsAdapter);
                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(CouponsActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {
        couponsAdapter.setOnItemClickListener(new CouponsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
//                Intent i = new Intent(CouponsActivity.this,CouponsDetailsActivity.class);
//                startActivity(i);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }


}