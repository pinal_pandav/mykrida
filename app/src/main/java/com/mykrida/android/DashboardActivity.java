package com.mykrida.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import me.ibrahimsn.lib.SmoothBottomBar;

public class DashboardActivity extends AppCompatActivity {

    NavController navController;
    SmoothBottomBar smoothBottomBar;
    View fragment;
    public ImageView imgFilter;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        getSupportActionBar().hide();
        smoothBottomBar = findViewById(R.id.bottomBar);
        fragment = findViewById(R.id.main_fragment);
        imgFilter = findViewById(R.id.imgFilter);
        navController = Navigation.findNavController(fragment);
        NavigationUI.setupActionBarWithNavController(this, navController);

        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
//                if (destination.getId() == R.id.second_fragment) {
//                    imgFilter.setVisibility(View.VISIBLE);
//                } else {
                    imgFilter.setVisibility(View.GONE);
//                }
            }
        });

        position = getIntent().getIntExtra("position",0);

        imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DashboardActivity.this, FilterConnectActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this).inflate(R.menu.menu, menu);
        smoothBottomBar.setupWithNavController(menu, navController);
        if(position == 0){
            navController.navigate(R.id.first_fragment);
        }else if(position == 1){
            navController.navigate(R.id.third_fragment);
        }else if(position == 2){
            navController.navigate(R.id.upcomingtournaments_fragment);
        }else if(position == 3){
            navController.navigate(R.id.upcomingevetns_fragment);
        }else if(position == 4){
            navController.navigate(R.id.fourth_fragment);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        navController.navigateUp();
        return super.onSupportNavigateUp();
    }
}