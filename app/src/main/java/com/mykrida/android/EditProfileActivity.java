package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.CenterMenuAdapter;
import com.mykrida.android.Adapter.SearchAdapter;
import com.mykrida.android.Adapter.SportsAdapter;
import com.mykrida.android.Model.ProfileModel;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.OnMenuItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    ImageView btnBack;
    EditText edtName,edtEmail,edtAddress,edtCity,edtPhoneNo;
    CardView btnUpdateProfile;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    KProgressHUD hud;
    ApiInterface apiService;
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    SharedPreferences.Editor editorUserData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);
        editorUserData = prefUserData.edit();
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews();

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        btnUpdateProfile.setOnClickListener(view -> {
            if(Validate()){
                String Name = edtName.getText().toString().trim();
                String Email = edtEmail.getText().toString().trim();
                String Address = edtAddress.getText().toString().trim();
                String City = edtCity.getText().toString().trim();
                String PhoneNo = edtPhoneNo.getText().toString().trim();

                UpdateProfile(Name,Email,Address,City,PhoneNo);
            }
        });

        GetProfile();

    }

    private void GetProfile() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetProfile(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            if (!jsonObject.getString("data").equals("null")) {
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                ProfileModel profileModel = new ProfileModel();
                                profileModel.setAddress(jsonObject1.getString("address"));
                                profileModel.setCity(jsonObject1.getString("city"));
                                profileModel.setEmail(jsonObject1.getString("email"));
                                profileModel.setName(jsonObject1.getString("name"));
                                profileModel.setPhoneno(jsonObject1.getString("phone_no"));
                                setData(profileModel);
                            } else {
                                Toast.makeText(EditProfileActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void UpdateProfile(String name, String email, String address, String city, String phoneNo) {

        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.UpdateProfile(Constant.APPKEY,name,email,city,phoneNo,address,city,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            if (!jsonObject.getString("data").equals("null")) {
                                editorUserData.putString("User_Info", jsonObject.getJSONObject("data").toString());
                                editorUserData.apply();
                                Toast.makeText(EditProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(EditProfileActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

    }

    private void setData(ProfileModel profileModel) {
        edtName.setText(profileModel.getName());
        edtAddress.setText(profileModel.getAddress());
        edtCity.setText(profileModel.getCity());
        edtEmail.setText(profileModel.getEmail());
        edtPhoneNo.setText(profileModel.getPhoneno());
    }

    private boolean Validate() {
        if(edtName.getText().toString().trim().isEmpty()){
            edtName.setError("Please enter Name");
            return false;
        }else if(edtEmail.getText().toString().trim().isEmpty()){
            edtEmail.setError("Please enter Email");
            return false;
        }else if(!edtEmail.getText().toString().trim().matches(emailPattern)){
            edtEmail.setError("Please enter valid Email");
            return false;
        }else if(edtAddress.getText().toString().trim().isEmpty()){
            edtAddress.setError("Please enter Address");
            return false;
        }else if(edtCity.getText().toString().trim().isEmpty()){
            edtCity.setError("Please enter City");
            return false;
        }else if(edtPhoneNo.getText().toString().trim().isEmpty()){
            edtPhoneNo.setError("Please enter Phone No");
            return false;
        }else if(edtPhoneNo.getText().toString().trim().length() != 10){
            edtPhoneNo.setError("Please enter valid Phone No");
            return false;
        }else{
            return true;
        }
    }


    private void findViews() {
        btnBack = findViewById(R.id.btnBack);
        edtName = findViewById(R.id.edtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtAddress = findViewById(R.id.edtAddress);
        edtCity = findViewById(R.id.edtCity);
        edtPhoneNo = findViewById(R.id.edtPhoneNo);
        btnUpdateProfile = findViewById(R.id.btnEditProfile);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
    }

}