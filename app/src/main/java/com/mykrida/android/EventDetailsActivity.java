package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.EventModel;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.SneakyThrows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsActivity extends Activity implements PaymentResultListener {

    TextView tvDate, tvTournamentName, tvTotalAmount, tvDescription;
    EditText edtNoOfTickets;
    ImageView btnBack;
    ImageView imgEvents;
    CardView btnBookEvent;
    ApiInterface apiService;
    KProgressHUD hud;
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    EventModel eventModel;
    String Name = "";
    String Email = "";
    String PhoneNumber = "";

    @SneakyThrows
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_details);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(EventDetailsActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        if (!jobjUser.get("email").isJsonNull()) {
            Email = jobjUser.get("email").getAsString();
        }
        if (!jobjUser.get("phone_no").isJsonNull()) {
            PhoneNumber = jobjUser.get("phone_no").getAsString();
        }
        if (!jobjUser.get("name").isJsonNull()) {
            Name = jobjUser.get("name").getAsString();
        }

        tvDate = findViewById(R.id.tvDate);
        tvTournamentName = findViewById(R.id.tvTournamentName);
        edtNoOfTickets = findViewById(R.id.edtNoOfTickets);
        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        tvDescription = findViewById(R.id.tvDescription);
        imgEvents = findViewById(R.id.imgEvents);
        btnBookEvent = findViewById(R.id.btnBookEvent);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
            onBackPressed();
        });

        Intent intent = getIntent();
        eventModel = intent.getParcelableExtra("eventdetails");
        assert eventModel != null;
        tvDate.setText(eventModel.getDate());
        setBookButtonVisibility(eventModel.getDate());
        tvTournamentName.setText(eventModel.getTitle());
        tvTotalAmount.setText("\u20B9 "+String.valueOf(eventModel.getPrice()));
        tvDescription.setText(eventModel.getDescription());
        edtNoOfTickets.setText("1");
        Glide.with(this)
                .load(eventModel.getImage())
                .centerCrop()
                .into(imgEvents);

        edtNoOfTickets.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tickets = s.toString();
                if (!tickets.isEmpty()) {
                    if (tickets.equals("0")) {
                        edtNoOfTickets.setText("1");
                    } else {
                        int totaltickets = Integer.parseInt(tickets) * eventModel.getPrice();
                        tvTotalAmount.setText(String.valueOf(totaltickets));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnBookEvent.setOnClickListener(v -> {
            //     BookEvent();
            startPayment();

        });



    }

    private void BookEvent() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.EventBooking(Constant.APPKEY, jobjUser.get("id").getAsInt(), eventModel.getId(), edtNoOfTickets.getText().toString(),
                tvTotalAmount.getText().toString().trim());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            Intent i = new Intent(EventDetailsActivity.this,MyEventsListActivity.class);
                            startActivity(i);
                            finish();
                            Toast.makeText(EventDetailsActivity.this, "Book Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(EventDetailsActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();
        co.setKeyID(getString(R.string.razorpaykey));

        try {
            JSONObject options = new JSONObject();
            options.put("name", "MyKrida");
            options.put("description", "MyKrida");
            options.put("send_sms_hash", true);
            options.put("allow_rotation", true);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", String.format("%.2f", Double.parseDouble(tvTotalAmount.getText().toString().trim().replace("\u20B9","").trim()) * 100));

            JSONObject preFill = new JSONObject();
            preFill.put("email", Email);
            preFill.put("contact", PhoneNumber);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Toast.makeText(this, "Payment Successful ", Toast.LENGTH_SHORT).show();

            BookEvent();
        } catch (Exception e) {
            Log.e("TAG", "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @Override
    public void onPaymentError(int code, String response) {
        Log.d("Error_payment_id", response);
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(response);
        if (jo.has("error")) {
            Toast.makeText(this, "" + jo.get("error").getAsJsonObject().get("description").getAsString(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "" + jo.get("description").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }

    public void setBookButtonVisibility(String bookingDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(bookingDate);
        Date date2 = Calendar.getInstance().getTime();

        System.out.println(sdf.format(date1));
        System.out.println(sdf.format(date2));
        System.out.println(""+date1.compareTo(date2));

        if(sdf.format(date1).equals(sdf.format(date2))){
            System.out.println("Date1 is equal to Date2");
            btnBookEvent.setVisibility(View.VISIBLE);
        }else if(date1.compareTo(date2)<0){
            System.out.println("Date1 is before Date2");
            btnBookEvent.setVisibility(View.GONE);
        }else if(date1.compareTo(date2)>0){
            System.out.println("Date1 is after Date2");
            btnBookEvent.setVisibility(View.VISIBLE);
        }else{
            System.out.println("How to get here?");
            btnBookEvent.setVisibility(View.VISIBLE);
        }

    }
}