package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.Model.EventDetailsModel;

public class EventDetailsShowActivity extends Activity  {

    TextView tvDate, tvTournamentName, tvTotalAmount, tvDescription;
    TextView edtNoOfTickets;
    ImageView btnBack;
    ImageView imgEvents;
    CardView btnBookEvent;
    KProgressHUD hud;
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    EventDetailsModel eventModel;
    String Name = "";
    String Email = "";
    String PhoneNumber = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_details_show);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(EventDetailsShowActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        if (!jobjUser.get("email").isJsonNull()) {
            Email = jobjUser.get("email").getAsString();
        }
        if (!jobjUser.get("phone_no").isJsonNull()) {
            PhoneNumber = jobjUser.get("phone_no").getAsString();
        }
        if (!jobjUser.get("name").isJsonNull()) {
            Name = jobjUser.get("name").getAsString();
        }

        tvDate = findViewById(R.id.tvDate);
        tvTournamentName = findViewById(R.id.tvTournamentName);
        edtNoOfTickets = findViewById(R.id.edtNoOfTickets);
        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        tvDescription = findViewById(R.id.tvDescription);
        imgEvents = findViewById(R.id.imgEvents);
        btnBookEvent = findViewById(R.id.btnBookEvent);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
            onBackPressed();
        });

        Intent intent = getIntent();
        eventModel = intent.getParcelableExtra("eventdetails");
        assert eventModel != null;
        tvDate.setText(eventModel.getDate());
        tvTournamentName.setText(eventModel.getTitle());
        tvTotalAmount.setText("\u20B9 "+String.valueOf(eventModel.getTot_amount()));
        tvDescription.setText(eventModel.getDescription());
        edtNoOfTickets.setText(String.valueOf(eventModel.getNo_of_ticket()));
        Glide.with(this)
                .load(eventModel.getImages())
                .centerCrop()
                .into(imgEvents);



    }


}