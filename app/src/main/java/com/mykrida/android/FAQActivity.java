package com.mykrida.android;

import android.app.Activity;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.mykrida.android.databinding.ActivityAboutUsBinding;
import com.mykrida.android.databinding.ActivityFaqBinding;

public class FAQActivity extends Activity {

    ActivityFaqBinding  binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_faq);

        binding.btnBack.setOnClickListener(view -> onBackPressed());

    }


}