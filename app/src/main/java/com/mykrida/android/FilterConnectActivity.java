package com.mykrida.android;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mykrida.android.Adapter.SportsAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;

public class FilterConnectActivity extends AppCompatActivity {

    RecyclerView rvSports,rvSkills;
    SportsAdapter sportsAdapter;
    ImageView btnBack;

    SportsAdapter skillsAdapter;
    ArrayList<String> arrSports = new ArrayList<>();
    ArrayList<String> arrSkills = new ArrayList<>();

    LinearLayout llStartTime,llEndTime;
    TextView tvDate;
    TextView tvStartTime,tvEndTime;
    Calendar myCalendar = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_connect);
        getSupportActionBar().hide();

        findViews();

        arrSports.add("Badminton");
        arrSports.add("Basketball");
        arrSports.add("Cricket");
        arrSports.add("Cricket Nets");
        arrSports.add("Cycling");
        arrSports.add("FootBall");
        arrSports.add("FootBall or Box Cricket");
        arrSports.add("Gyming");
        arrSports.add("Kayaking");
        arrSports.add("Lawn Tennis");
        arrSports.add("Multi Sport");
        arrSports.add("Running");
        arrSports.add("Squash");
        arrSports.add("Table Tennis");
        arrSports.add("Tennis");
        arrSports.add("Trekking");
        arrSports.add("Volleyball");

        arrSkills.add("Beginner");
        arrSkills.add("Amateur");
        arrSkills.add("Intermediate");
        arrSkills.add("Adavanced");
        arrSkills.add("Professional");


        rvSports.setHasFixedSize(true);
        rvSports.setLayoutFrozen(true);
        rvSports.setNestedScrollingEnabled(false);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvSports.setLayoutManager(llmF);

        // TODO: 05-01-2021 Set data in adapter
        sportsAdapter = new SportsAdapter(this, "Fragment", arrSports, "");
        rvSports.setAdapter(sportsAdapter);

        sportsAdapter.setOnItemClickListener(new SportsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

        rvSkills.setHasFixedSize(true);
        rvSkills.setLayoutFrozen(true);
        rvSkills.setNestedScrollingEnabled(false);
        LinearLayoutManager llm = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvSkills.setLayoutManager(llm);

        // TODO: 05-01-2021 Set data in adapter
        skillsAdapter = new SportsAdapter(this, "Fragment", arrSkills, "");
        rvSkills.setAdapter(skillsAdapter);

        skillsAdapter.setOnItemClickListener(new SportsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {

            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog datePicker = new DatePickerDialog(FilterConnectActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));

                datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePicker.show();
            }
        });

        llStartTime.setOnClickListener(view -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(FilterConnectActivity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    tvStartTime.setText(selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });

        llEndTime.setOnClickListener(view -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(FilterConnectActivity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    tvEndTime.setText(selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });




    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }



    };

    private void updateLabel() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDate.setText(sdf.format(myCalendar.getTime()));
    }



    private void findViews() {
        rvSports = findViewById(R.id.rvSports);
        rvSkills = findViewById(R.id.rvSkills);
        btnBack = findViewById(R.id.btnBack);
        llStartTime = findViewById(R.id.llStartTime);
        llEndTime = findViewById(R.id.llEndTime);
        tvDate = findViewById(R.id.tvDate);
        tvStartTime = findViewById(R.id.tvStartTime);
        tvEndTime = findViewById(R.id.tvEndTime);
    }

}