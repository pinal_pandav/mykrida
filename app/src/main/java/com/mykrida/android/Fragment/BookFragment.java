package com.mykrida.android.Fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.AroundCityAdapter2;
import com.mykrida.android.Adapter.CategoryAdapter;
import com.mykrida.android.Adapter.CenterMenuAdapter;
import com.mykrida.android.Adapter.CouponsAdapter;
import com.mykrida.android.Adapter.DiscountAndOffersAdapter;
import com.mykrida.android.Adapter.ImageSliderAdapter;
import com.mykrida.android.ChangeLocationActivity;
import com.mykrida.android.CouponsActivity;
import com.mykrida.android.Model.Category;
import com.mykrida.android.Model.CategoryList;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.Model.ImageSliderModel;
import com.mykrida.android.Model.TrufModel;
import com.mykrida.android.R;
import com.mykrida.android.SearchActivity;
import com.mykrida.android.SportsDetailActivity;
import com.skydoves.powermenu.CircularEffect;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BookFragment extends Fragment {

    RecyclerView rvDiscountAndOffers,rvCategory,rvAroundCity;
    private CustomPowerMenu<String, CenterMenuAdapter> writeMenu;
    TextView tvSports, tvDate;
    LinearLayout llSelectSport;
    Calendar myCalendar = Calendar.getInstance();
    ArrayList<Integer> arrImages = new ArrayList<>();
    CardView btnSearch;
    KProgressHUD hud;
    Context mContext;
    ApiInterface apiService;
    String strSelectedDate;
    int strCategoryID;
    ArrayList<CategoryList> arrCategory = new ArrayList<>();
    ArrayList<Category> arrCategoryNew = new ArrayList<>();
    ArrayList<CouponModel> arrCoupons = new ArrayList<>();
    ArrayList<TrufModel> arrSports = new ArrayList<>();
    DiscountAndOffersAdapter discountAndOffersAdapter;
    AroundCityAdapter2 aroundCityAdapter;
    CategoryAdapter categoryAdapter;
    SharedPreferences prefUserData;
    String Location;
    TextView tvNoDataFoundCity;
    public SliderView svImage;
    public ArrayList<ImageSliderModel> arrSliderImage_Bottom = new ArrayList<>();
    TextView tvDiscoundAndOffer;
    public static boolean isCalled = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_book, container, false);

        mContext = getActivity();

        prefUserData = mContext.getSharedPreferences("USERDATA",MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info","");
        Location = prefUserData.getString("location","N/A");

        tvNoDataFoundCity = v.findViewById(R.id.tvNoDataFoundCity);

        tvSports = v.findViewById(R.id.tvSports);
        llSelectSport = v.findViewById(R.id.llSports);
        tvDate = v.findViewById(R.id.tvDate);
        btnSearch = v.findViewById(R.id.btnSearch);
        rvCategory = v.findViewById(R.id.rvCategory);
        tvDiscoundAndOffer = v.findViewById(R.id.tvDiscountAndOffer);
        svImage = v.findViewById(R.id.imageSlider_top);



        hud = KProgressHUD.create(mContext)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvDiscountAndOffers = v.findViewById(R.id.rvDiscountAndOffers);
        rvDiscountAndOffers.setHasFixedSize(true);
        rvDiscountAndOffers.setLayoutFrozen(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvDiscountAndOffers.setLayoutManager(llm);

        GridLayoutManager llm1 = new GridLayoutManager(getActivity(), 2);
        rvCategory.setLayoutManager(llm1);

        rvAroundCity = v.findViewById(R.id.rvAroundCity);
        rvAroundCity.setHasFixedSize(true);
        rvAroundCity.setLayoutFrozen(true);
        LinearLayoutManager llm2 = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvAroundCity.setLayoutManager(llm2);

        GetCategoryList();

        tvSports.setOnClickListener(view -> {
            if(writeMenu != null) {
                if (writeMenu.isShowing()) {
                    writeMenu.dismiss();
                    return;
                }
                writeMenu.showAtCenter(v);
            }
        });

        tvDate.setOnClickListener(view -> {

            DatePickerDialog datePicker = new DatePickerDialog(getActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Location.equalsIgnoreCase("N/A")){
                    Toast.makeText(getActivity(), "Please Select Location from Profile", Toast.LENGTH_SHORT).show();
                }else {
                    if (tvSports.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getActivity(), "Please select any Sport", Toast.LENGTH_SHORT).show();
                    } else if (tvDate.getText().toString().trim().isEmpty()) {
                        Toast.makeText(getActivity(), "Please select Date", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent i = new Intent(getActivity(), SearchActivity.class);
                        i.putExtra("date", strSelectedDate);
                        i.putExtra("id", strCategoryID);
                        i.putExtra("catName", tvSports.getText().toString());
                        i.putExtra("strDate", tvDate.getText().toString());
                        i.putExtra("isFromSearch",true);
                        startActivity(i);
                    }
                }
            }
        });

        return v;
    }

    private final OnMenuItemClickListener<String> onWriteItemClickListener =
            new OnMenuItemClickListener<String>() {
                @Override
                public void onItemClick(int position, String title) {
                    tvSports.setText(title);
                    strCategoryID = arrCategory.get(position).getId();
                    writeMenu.dismiss();
                }
            };

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    @Override
    public void onStart() {
        super.onStart();
        Location = prefUserData.getString("location","N/A");
        GetAroundCity(false);
    }

    private void updateLabel() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDate.setText(sdf.format(myCalendar.getTime()));

        String myFormatforAPI = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
        strSelectedDate = sdfforAPI.format(myCalendar.getTime());
    }

    public CustomPowerMenu<String, CenterMenuAdapter> getWritePowerMenu(
            Context context,
            LifecycleOwner lifecycleOwner,
            OnMenuItemClickListener<String> onMenuItemClickListener,ArrayList<CategoryList> arrayList) {
        ColorDrawable drawable = new ColorDrawable(ContextCompat.getColor(context, R.color.black));

        CustomPowerMenu.Builder<String, CenterMenuAdapter> builder = new CustomPowerMenu.Builder<String, CenterMenuAdapter>(context,new CenterMenuAdapter());
        for(int i =0;i<arrayList.size();i++){
            builder.addItem(arrayList.get(i).getcName());
        }
        builder.setLifecycleOwner(lifecycleOwner);
        builder.setAnimation(MenuAnimation.FADE);
        builder.setCircularEffect(CircularEffect.BODY);
        builder.setMenuRadius(10f);
        builder.setMenuShadow(10f);
        builder.setDivider(drawable);
        builder.setDividerHeight(1);
        builder.setOnMenuItemClickListener(onMenuItemClickListener);

        return builder.build();
    }

    private void GetCategoryList() {
        hud.show();
        isCalled = true;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetCategoryList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    if (arrCategory.size() == 0) {
                        try {
                            arrCategory.clear();
                            arrCategoryNew.clear();
                            JSONObject jsonObject = new JSONObject(response.body());
                            if (jsonObject.getString("status").equals("1")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    CategoryList categoryList = new CategoryList(jsonObject1.getString("cname"),
                                            jsonObject1.getString("date"), jsonObject1.getInt("id"), jsonObject1.getInt("isdeleted"));
                                    arrCategory.add(categoryList);
                                    Category category = new Category();
                                    category.setName(jsonObject1.getString("cname"));
                                    category.setImage(jsonObject1.getString("image"));
                                    category.setId(jsonObject1.getInt("id"));
                                    arrCategoryNew.add(category);
                                }

                            } else {
                                Toast.makeText(mContext, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            hud.dismiss();
                            e.printStackTrace();
                        }
                    }
                    SetDataInPopup(arrCategory);
                    if (Location.equalsIgnoreCase("N/A")) {
                        Toast.makeText(getActivity(), "Please Select Location from Profile", Toast.LENGTH_SHORT).show();
                        tvNoDataFoundCity.setText("Please select Location from Profile");
                        tvNoDataFoundCity.setVisibility(View.VISIBLE);
                        GetCouponList();
                    } else {
                        GetAroundCity(true);
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });


    }

    private void SetDataInPopup(ArrayList<CategoryList> arrCategory) {
        writeMenu = getWritePowerMenu(getActivity(), this, onWriteItemClickListener,arrCategory);

        if(arrCategoryNew.size() != 0) {
            categoryAdapter = new CategoryAdapter(getActivity(), "Fragment", arrCategoryNew, "");
            rvCategory.setAdapter(categoryAdapter);

            categoryAdapter.setOnItemClickListener(new CategoryAdapter.ClickListener() {
                @Override
                public void onItemClick(int position, View v) {

                    Calendar myCalendar = Calendar.getInstance();
                    String myFormatforAPI = "yyyy-MM-dd"; //In which you need put here
                    SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
                    String strSelectedDate = sdfforAPI.format(myCalendar.getTime());

                    String myFormat = "dd MMM yyyy"; //In which you need put here
                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                    String date = sdf.format(myCalendar.getTime());

                    Intent i = new Intent(getActivity(), SearchActivity.class);
                    i.putExtra("date", strSelectedDate);
                    i.putExtra("id", arrCategoryNew.get(position).getId());
                    i.putExtra("catName", arrCategoryNew.get(position).getName());
                    i.putExtra("strDate", date);
                    i.putExtra("isFromSearch",false);
                    startActivity(i);

                }

                @Override
                public void onItemLongClick(int position, View v) {

                }
            });
        }


        tvNoDataFoundCity.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ChangeLocationActivity.class);
            startActivity(i);
        });
    }

    private void GetCouponList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetCoupons(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrCoupons.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                CouponModel couponModel = new CouponModel();
                                couponModel.setCoupon_code(jsonObject1.getString("code"));
                                couponModel.setDiscount(jsonObject1.getInt("discount"));
                                couponModel.setEnd_date(jsonObject1.getString("end_date"));
                                couponModel.setId(jsonObject1.getInt("id"));
                                couponModel.setImage(jsonObject1.getString("image"));
                                couponModel.setStart_date(jsonObject1.getString("start_date"));
                                arrCoupons.add(couponModel);

                            }

                            if(arrCoupons.size() != 0) {
                                discountAndOffersAdapter = new DiscountAndOffersAdapter(getActivity(), "Fragment", arrCoupons, "");
                                rvDiscountAndOffers.setAdapter(discountAndOffersAdapter);

                                setAdapterClick();
                            }else{
                                tvDiscoundAndOffer.setVisibility(View.GONE);
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }

                }
                GetSliderAPI();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void GetSliderAPI() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetSlider(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                arrSliderImage_Bottom.add(new ImageSliderModel(jsonObject.getJSONArray("data").getJSONObject(i).getString("image")));

                            }
                            if(jsonObject.getJSONArray("data").length() == 0){
                                svImage.setVisibility(View.GONE);
                            }
                            ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(getActivity(), arrSliderImage_Bottom);
                            svImage.setSliderAdapter(imageSliderAdapter);
                            svImage.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                            svImage.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                            svImage.setScrollTimeInSec(2); //set scroll delay in seconds :
                            svImage.startAutoCycle();

                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });

    }

    private void GetAroundCity(boolean b) {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetAroundCity(Constant.APPKEY,Location);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrSports.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                TrufModel trufModel = new TrufModel();
                                trufModel.setId(jsonObject1.getInt("id"));
                                trufModel.setCid(jsonObject1.getInt("cid"));
                                trufModel.setScid(jsonObject1.getInt("scid"));
                                trufModel.setVid(jsonObject1.getInt("vid"));
                                trufModel.setTname(jsonObject1.getString("tname"));
                                trufModel.setAddress(jsonObject1.getString("address"));
                                trufModel.setDescription(jsonObject1.getString("descr"));
                                trufModel.setProcingset(jsonObject1.getString("rate"));
                                trufModel.setPrice(jsonObject1.getString("rate"));
                                trufModel.setTrufID(jsonObject1.getInt("id"));
//                            trufModel.setDay(jsonObject1.getString("day"));
//                            trufModel.setFromtime(jsonObject1.getString("from_time"));
//                            trufModel.setTotime(jsonObject1.getString("to_time"));
                                trufModel.setImages(jsonObject1.getString("images"));
//                            trufModel.setTrufID(jsonObject1.getInt("turf_id"));


                                /*SportsModel sportsModel = new SportsModel();
                                sportsModel.setImage(jsonObject1.getString("images"));
                                sportsModel.setTname(jsonObject1.getString("tname"));*/
                                arrSports.add(trufModel);
                            }

                            if(arrSports.size() != 0) {
                                aroundCityAdapter = new AroundCityAdapter2(getActivity(), "Fragment", arrSports, "");
                                rvAroundCity.setAdapter(aroundCityAdapter);
                                tvNoDataFoundCity.setVisibility(View.GONE);
                            }else{
                                tvNoDataFoundCity.setText("No Data Found");
                                tvNoDataFoundCity.setVisibility(View.VISIBLE);
                            }

                            if(aroundCityAdapter != null) {
                                aroundCityAdapter.setOnItemClickListener(new AroundCityAdapter2.ClickListener() {
                                    @Override
                                    public void onItemClick(int position, View v) {
                                        Calendar myCalendar = Calendar.getInstance();
                                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                        String Date = sdf.format(myCalendar.getTime());
                                        Intent i = new Intent(getActivity(), SportsDetailActivity.class);
                                        i.putExtra("trufdata", arrSports.get(position));
                                        i.putExtra("date", Date);
                                        startActivity(i);
                                    }

                                    @Override
                                    public void onItemLongClick(int position, View v) {

                                    }
                                });
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            tvNoDataFoundCity.setText("No Data Found");
                            tvNoDataFoundCity.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                    if(b) {
                        GetCouponList();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
                if(b) {
                    GetCouponList();
                }
            }
        });
    }

    private void setAdapterClick() {
        discountAndOffersAdapter.setOnItemClickListener(new CouponsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent i = new Intent(getActivity(), CouponsActivity.class);
                startActivity(i);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }


}