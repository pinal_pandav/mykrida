package com.mykrida.android.Fragment;

import static android.content.Context.MODE_PRIVATE;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.ajithvgiri.searchdialog.SearchListItem;
import com.ajithvgiri.searchdialog.SearchableDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BulkBookingFragment extends Fragment {

    //    TextView tvNumber;
    CardView btnBookNow;
    TextView edtTrufID;
    TextView edtFromTime, edtToTime, edtFromDate, edtToDate;
    Calendar myCalendar = Calendar.getInstance();
    String strSelectedFromDate,strSelectedToDate;
    ApiInterface apiService;
    KProgressHUD hud;
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    int TurfID;

    SearchableDialog dialogTruf;
    ArrayList<SearchListItem> arrTruf = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bulk_booking, container, false);
        edtTrufID = v.findViewById(R.id.edtTrufID);
        edtFromDate = v.findViewById(R.id.edtFromDate);
        edtToDate = v.findViewById(R.id.edtToDate);
        edtFromTime = v.findViewById(R.id.edtFromTime);
        edtToTime = v.findViewById(R.id.edtToTime);

        prefUserData = getActivity().getSharedPreferences("USERDATA", MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        btnBookNow = v.findViewById(R.id.btnBookNow);

        dialogTruf = new SearchableDialog(getActivity(), arrTruf, "Select Truf");


        btnBookNow.setOnClickListener(view -> {
            if (Validate()) {
                BulkBooking();
            }
        });

        edtFromTime.setOnClickListener(view -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    edtFromTime.setText(selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });

        edtToTime.setOnClickListener(view -> {
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    edtToTime.setText(selectedHour + ":" + selectedMinute);
                }
            }, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();
        });

        edtFromDate.setOnClickListener(view -> {
            Calendar myCalendar = Calendar.getInstance();
            DatePickerDialog datePicker = new DatePickerDialog(getActivity(), date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();
        });

        edtToDate.setOnClickListener(view -> {
            Calendar myCalendar = Calendar.getInstance();
            DatePickerDialog datePicker = new DatePickerDialog(getActivity(), dateToDate, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();
        });

        edtTrufID.setOnClickListener(v1 -> {
            dialogTruf.show();
        });



        GetTurfDetails();

        return v;
    }

    private void BulkBooking() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.BulkBooking(Constant.APPKEY,
                jobjUser.get("id").getAsInt(),
                String.valueOf(TurfID),
                edtFromTime.getText().toString().trim(),
                edtToTime.getText().toString().trim(),
                strSelectedFromDate,
                strSelectedToDate);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        ClearData();
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void GetTurfDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.TurfList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                arrTruf.clear();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if(jsonObject.getString("status").equals("1")){
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for(int i = 0;i<jsonArray.length();i++){
                                arrTruf.add(new SearchListItem(jsonArray.getJSONObject(i).getInt("id"),
                                        jsonArray.getJSONObject(i).getString("tname")));
                            }
                        }
                        dialogTruf = new SearchableDialog(getActivity(), arrTruf, "Select Truf");

                        dialogTruf.setOnItemSelected((position, searchListItem) -> {
                            edtTrufID.setText(searchListItem.getTitle());
                            TurfID = searchListItem.getId();
                        });
                        //Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void ClearData() {
        edtTrufID.setText("");
        edtFromDate.setText("");
        edtToTime.setText("");
        edtFromTime.setText("");
        edtToDate.setText("");
        edtTrufID.requestFocus();
    }

    private boolean Validate() {
        String TrufID = edtTrufID.getText().toString().trim();
        String FromTime = edtFromTime.getText().toString().trim();
        String ToTime = edtToTime.getText().toString().trim();
        String FromDate = edtFromDate.getText().toString().trim();
        String ToDate = edtToDate.getText().toString().trim();

        if (TrufID.isEmpty()) {
            Toast.makeText(getActivity(), "Please select Truf", Toast.LENGTH_SHORT).show();
            return false;
        }else if (FromTime.isEmpty()) {
            Toast.makeText(getActivity(), "Please select From Time", Toast.LENGTH_SHORT).show();
            return false;
        } else if (ToTime.isEmpty()) {
            Toast.makeText(getActivity(), "Please select To Time", Toast.LENGTH_SHORT).show();
            return false;
        } else if (FromDate.isEmpty()) {
            Toast.makeText(getActivity(), "Please select From Date", Toast.LENGTH_SHORT).show();
            return false;
        } else if (ToDate.isEmpty()) {
            Toast.makeText(getActivity(), "Please select To Date", Toast.LENGTH_SHORT).show();
            return false;
        }  else {
            return true;
        }
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private void updateLabel() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edtFromDate.setText(sdf.format(myCalendar.getTime()));

        String myFormatforAPI = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
        strSelectedFromDate = sdfforAPI.format(myCalendar.getTime());
    }

    DatePickerDialog.OnDateSetListener dateToDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabelToDate();
        }

    };

    private void updateLabelToDate() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edtToDate.setText(sdf.format(myCalendar.getTime()));

        String myFormatforAPI = "yyyy/MM/dd"; //In which you need put here
        SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
        strSelectedToDate = sdfforAPI.format(myCalendar.getTime());
    }


}