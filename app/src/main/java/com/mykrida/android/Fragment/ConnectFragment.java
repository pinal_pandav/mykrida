package com.mykrida.android.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.mykrida.android.Adapter.ViewPagerAdapter;
import com.mykrida.android.R;

public class ConnectFragment extends Fragment {

    ViewPager viewPager;
    TabLayout tabLayout;
    GamesAroundYouFragment gamesAroundYouFragment;
    GamesByYouFragment gamesByYouFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_connect, container, false);

        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);

        //Initializing the tablayout
        tabLayout = (TabLayout) v.findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position,false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        setupViewPager(viewPager);

        return v;
    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        gamesAroundYouFragment=new GamesAroundYouFragment();
        gamesByYouFragment=new GamesByYouFragment();
        adapter.addFragment(gamesAroundYouFragment,"Games Around You");
        adapter.addFragment(gamesByYouFragment,"Games By You");
        viewPager.setAdapter(adapter);
    }

}