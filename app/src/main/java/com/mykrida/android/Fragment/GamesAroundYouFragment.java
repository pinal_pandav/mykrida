package com.mykrida.android.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mykrida.android.R;

public class GamesAroundYouFragment extends Fragment {

    FloatingActionButton btnCreateGame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_games_around_you, container, false);

        btnCreateGame = v.findViewById(R.id.btnCreateGame);

        return v;
    }

}