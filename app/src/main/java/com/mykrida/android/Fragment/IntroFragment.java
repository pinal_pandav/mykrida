package com.mykrida.android.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.mykrida.android.R;

public class IntroFragment extends Fragment {

    TextView tvTitle,tvDescription;
    ImageView imgIntro;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.item_viewpager_intro, container, false);

        tvTitle = v.findViewById(R.id.tvTitle);
        tvDescription = v.findViewById(R.id.tvDescription);
        imgIntro = v.findViewById(R.id.imgIntro);

        tvTitle.setText(getArguments().getString("title"));
        tvDescription.setText(getArguments().getString("description"));
        imgIntro.setImageResource(getArguments().getInt("image"));

        return v;
    }

    public static IntroFragment newInstance(String title, String description, int image) {

        IntroFragment f = new IntroFragment();
        Bundle b = new Bundle();
        b.putString("title", title);
        b.putString("description",description);
        b.putInt("image",image);
        f.setArguments(b);

        return f;
    }
}