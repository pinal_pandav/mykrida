package com.mykrida.android.Fragment;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.BookSportsActivity;
import com.mykrida.android.Model.TrufModel;
import com.mykrida.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OverviewFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    CardView btnBookNow;
    KProgressHUD hud;
    Context mContext;
    ApiInterface apiService;
    int TrufID;
    ImageView imgTruf;
    TextView tvDescription,tvAddress1;
    String Date;
    TrufModel trufModel = new TrufModel();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_overview, container, false);

        mContext = getActivity();

        TrufID  = getArguments().getInt("trufID");
        Date = getArguments().getString("Date");
        hud = KProgressHUD.create(mContext)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews(v);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        btnBookNow = v.findViewById(R.id.btnBookNow);
        btnBookNow.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), BookSportsActivity.class);
            i.putExtra("price",trufModel.getPrice());
            i.putExtra("trufID",TrufID);
            i.putExtra("data",trufModel);
            i.putExtra("Date",Date);
            startActivity(i);
        });

        GetTrufDetails(TrufID);



        return v;
    }

    private void findViews(View v) {
        tvDescription = v.findViewById(R.id.tvDescription);
        tvAddress1 = v.findViewById(R.id.tvAddress1);
        imgTruf = v.findViewById(R.id.imgTruf);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        Geocoder coder = new Geocoder(getActivity());
        List<Address> address;
        try {
            address = coder.getFromLocationName(Address, 5);
            if (address != null) {

                Address location = address.get(0);
                location.getLatitude();
                location.getLongitude();

                LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(sydney)
                        .title("Marker in "+Address));
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16f));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    String Address = "";

    private void GetTrufDetails(int TrufID) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetTrufDetails(Constant.APPKEY,TrufID);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            trufModel.setId(jsonObject1.getInt("id"));
                            trufModel.setCid(jsonObject1.getInt("cid"));
                            trufModel.setScid(jsonObject1.getInt("scid"));
                            trufModel.setVid(jsonObject1.getInt("vid"));
                            trufModel.setTname(jsonObject1.getString("tname"));
                            trufModel.setAddress(jsonObject1.getString("address"));
                            Address = jsonObject1.getString("address");
                            Geocoder coder = new Geocoder(getActivity());
                            List<Address> address;
                            try {
                                address = coder.getFromLocationName(jsonObject1.getString("address"), 5);
                                if (address != null) {

                                    Address location = address.get(0);
                                    location.getLatitude();
                                    location.getLongitude();

                                    LatLng sydney = new LatLng(location.getLatitude(), location.getLongitude());
                                    mMap.addMarker(new MarkerOptions()
                                            .position(sydney)
                                            .title("Marker in "+jsonObject1.getString("address")));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16f));

                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            trufModel.setDescription(jsonObject1.getString("descr"));
                            trufModel.setProcingset(jsonObject1.getString("rate"));
                            trufModel.setPrice(jsonObject1.getString("rate"));
//                            trufModel.setDay(jsonObject1.getString("day"));
//                            trufModel.setFromtime(jsonObject1.getString("from_time"));
//                            trufModel.setTotime(jsonObject1.getString("to_time"));
                            trufModel.setImages(jsonObject1.getString("images"));
//                            trufModel.setTrufID(jsonObject1.getInt("turf_id"));
                            setData();

                        }else{
                            Toast.makeText(mContext, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setData() {
        tvDescription.setText(trufModel.getDescription());
        tvAddress1.setText(trufModel.getAddress().replace(",","\n").trim());
        Glide.with(getActivity()).load(trufModel.getImages()).centerCrop().into(imgTruf);
    }


    public OverviewFragment getInstant(int trufID,String Date) {
        OverviewFragment f = new OverviewFragment();
        Bundle b = new Bundle();
        b.putInt("trufID", trufID);
        b.putString("Date",Date);
        f.setArguments(b);

        return f;
    }
}