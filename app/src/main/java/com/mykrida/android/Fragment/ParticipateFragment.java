package com.mykrida.android.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mykrida.android.Adapter.HappeningSoonAdapter;
import com.mykrida.android.Adapter.ParticipateAdapter;
import com.mykrida.android.ChangeLocationActivity;
import com.mykrida.android.R;

public class ParticipateFragment extends Fragment {

    RecyclerView rvParticipate,rvHappeningSoon;
    TextView btnChange;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_participate, container, false);

        btnChange = v.findViewById(R.id.btnChange);

        rvParticipate = v.findViewById(R.id.rvParticipates);
        rvParticipate.setHasFixedSize(true);
        rvParticipate.setLayoutFrozen(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvParticipate.setLayoutManager(llm);

        // TODO: 05-01-2021 Set data in adapter
        ParticipateAdapter participateAdapter = new ParticipateAdapter(getActivity(), "Fragment", null, "");
        rvParticipate.setAdapter(participateAdapter);

        rvHappeningSoon = v.findViewById(R.id.rvHappeningSoon);
        rvHappeningSoon.setHasFixedSize(true);
        rvHappeningSoon.setLayoutFrozen(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        rvHappeningSoon.setLayoutManager(gridLayoutManager);

        // TODO: 05-01-2021 Set data in adapter
        HappeningSoonAdapter happeningSoonAdapter = new HappeningSoonAdapter(getActivity(), "Fragment", null, "");
        rvHappeningSoon.setAdapter(happeningSoonAdapter);

        btnChange.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ChangeLocationActivity.class);
            startActivity(i);
        });

        return v;
    }

}