package com.mykrida.android.Fragment;

import static android.content.Context.MODE_PRIVATE;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.AboutUsActivity;
import com.mykrida.android.Adapter.UpcomingEventsAdapter;
import com.mykrida.android.AthletesListFragment;
import com.mykrida.android.BookingListActivity;
import com.mykrida.android.ChangeLocationActivity;
import com.mykrida.android.ContactUsActivity;
import com.mykrida.android.CouponsActivity;
import com.mykrida.android.DashboardActivity;
import com.mykrida.android.EditProfileActivity;
import com.mykrida.android.FAQActivity;
import com.mykrida.android.LoginActivity;
import com.mykrida.android.Model.AtheleData.AtheleResponse;
import com.mykrida.android.Model.EventModel;
import com.mykrida.android.Model.tournaments.TournamentsResponse;
import com.mykrida.android.Model.wallet_amount.WalletAmountResponse;
import com.mykrida.android.MyEventsListActivity;
import com.mykrida.android.MyTournamentListActivity;
import com.mykrida.android.R;
import com.mykrida.android.UpComingEventActivity;
import com.mykrida.android.WebviewActivity;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    ImageView btnEditProfile;
    TextView btnChange;
    RelativeLayout rlCoupon,rlBooking,rlLogout,rlEvents,rlTournaments,rlAllEvents,rlWallet;
    ApiInterface apiService;
    KProgressHUD hud;
    DashboardActivity dashboardActivity;
    SharedPreferences prefUserData;
    SharedPreferences.Editor editorUserData;
    JsonObject jobjUser;
    TextView tvBookingCount,tvEventsCount;
    TextView tvLocation,tvUsername,tvWalletAmount,tvTournamentsCount,tvCoupleCount,tvAtheleCount;
    RelativeLayout rlContactUs,rlPrivacyPolicy,rlTermsAndCondition,rlFAQ,rlAboutUs;

    @Override
    public void onStart() {
        super.onStart();
        String Location = prefUserData.getString("location","N/A");
        tvLocation.setText(Location);
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);
        if(!jobjUser.get("name").isJsonNull()) {
            tvUsername.setText(jobjUser.get("name").getAsString());
        }else{
            tvUsername.setText("");
        }
        GetWallet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        dashboardActivity = (DashboardActivity) getActivity();

        prefUserData = dashboardActivity.getSharedPreferences("USERDATA",MODE_PRIVATE);
        editorUserData = prefUserData.edit();
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        String Location = prefUserData.getString("location","N/A");

        btnEditProfile = v.findViewById(R.id.btnEditProfile);
        btnChange = v.findViewById(R.id.btnChange);
        rlCoupon = v.findViewById(R.id.rlCoupon);
        rlBooking = v.findViewById(R.id.rlBooking);
        rlEvents = v.findViewById(R.id.rlEvents);
        tvBookingCount = v.findViewById(R.id.tvBookingCount);
        tvEventsCount = v.findViewById(R.id.tvEventsCount);
        rlLogout = v.findViewById(R.id.rlLogout);
        tvLocation = v.findViewById(R.id.tvLocation);
        tvLocation.setText(Location);
        tvUsername = v.findViewById(R.id.tvUserName);
        rlAllEvents = v.findViewById(R.id.rlAllEvents);
        rlWallet = v.findViewById(R.id.rlWallet);
        rlTournaments = v.findViewById(R.id.rlTournaments);

        rlAboutUs = v.findViewById(R.id.rlAboutKrida);
        rlTermsAndCondition = v.findViewById(R.id.rlTermsAndCondition);
        rlContactUs = v.findViewById(R.id.rlContactUs);
        rlFAQ = v.findViewById(R.id.rlFAQ);
        rlPrivacyPolicy = v.findViewById(R.id.rlPrivacyPolicy);

        tvWalletAmount = v.findViewById(R.id.tvWalletAmount);
        tvCoupleCount = v.findViewById(R.id.tvCoupleCount);
        tvAtheleCount = v.findViewById(R.id.tvAtheleCount);
        tvTournamentsCount = v.findViewById(R.id.tvTournamentsCount);



        if(!jobjUser.get("name").isJsonNull()) {
            tvUsername.setText(jobjUser.get("name").getAsString());
        }else{
            tvUsername.setText("");
        }

        hud = KProgressHUD.create(dashboardActivity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        btnEditProfile.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), EditProfileActivity.class);
            startActivity(i);
        });


        rlAllEvents.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), UpComingEventActivity.class);
            startActivity(i);
        });

        btnChange.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ChangeLocationActivity.class);
            startActivity(i);
        });

        rlCoupon.setOnClickListener(view -> {
                Intent i = new Intent(getActivity(), CouponsActivity.class);
                i.putExtra("Apply",false);
                startActivity(i);
        });

        rlBooking.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), BookingListActivity.class);
            startActivity(i);
        });

        rlEvents.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), MyEventsListActivity.class);
            startActivity(i);
        });


        rlLogout.setOnClickListener(view -> {
            editorUserData.putBoolean("isLoggedIn",false);
            editorUserData.apply();

            Intent intent = new Intent(getActivity(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            getActivity().finish();

        });

        rlPrivacyPolicy.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), WebviewActivity.class);
            i.putExtra("title","Privacy Policy");
            i.putExtra("url","http://mykrida.com/privacy-policy");
            startActivity(i);
        });

        rlFAQ.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), FAQActivity.class);
            startActivity(i);
        });

        rlContactUs.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ContactUsActivity.class);
            startActivity(i);
        });

        rlTermsAndCondition.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), WebviewActivity.class);
            i.putExtra("title","Terms & Conditions");
            i.putExtra("url","http://mykrida.com/terms-condition");
            startActivity(i);
        });

        rlAboutUs.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), AboutUsActivity.class);
            startActivity(i);
        });

        rlTournaments.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), MyTournamentListActivity.class);
            startActivity(i);
        });

        GetBookingDetails();
        return v;
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetBookingDetails(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            tvBookingCount.setText(String.valueOf(jsonObject.getJSONArray("data").length()));
                        } else {
                            Toast.makeText(dashboardActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
                GetEventsList();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
                GetEventsList();
            }
        });
    }

    private void GetEventsList() {

        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetMyEventsList(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            tvEventsCount.setText(String.valueOf(jsonObject.getJSONArray("data").length()));
                        } else {
                            Toast.makeText(dashboardActivity, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
                GetWallet();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
                GetWallet();
            }
        });
    }

    private void GetWallet() {
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<WalletAmountResponse> callCard = apiService.getWalletAmount(Constant.APPKEY, jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus() == 1) {
                        if(response.body().getData() != null) {
                            tvWalletAmount.setText("\u20B9 " +response.body().getData().getAmount());
                        }else{
                            tvWalletAmount.setText("\u20B9 " +"0");
                        }
                    }

                }
                GetCouponList();
            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                hud.dismiss();
                GetCouponList();
            }


        });
    }

    private void GetCouponList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetCoupons(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        int count = jsonObject.getJSONArray("data").length();
                        tvCoupleCount.setText(String.valueOf(count));
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
                getEventList();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
                getEventList();
            }
        });
    }

    private void getEventList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetEventList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            if(jsonObject.has("data")) {
                               String length = String.valueOf(jsonObject.getJSONArray("data").length());
                               tvAtheleCount.setText(length);
                            }
                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
                getTournaments();
            }

            @Override
            public void onFailure (Call < String > call, Throwable t){
                getTournaments();
                hud.dismiss();
            }
        });
    }

    private void getTournaments() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TournamentsResponse> callCard = apiService.getTournaments(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<TournamentsResponse>() {
            @Override
            public void onResponse(Call<TournamentsResponse> call, Response<TournamentsResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        tvTournamentsCount.setText(String.valueOf(response.body().getData().size()));
                    }
                }
            }

            @Override
            public void onFailure (Call <TournamentsResponse> call, Throwable t){
                hud.dismiss();
            }
        });
    }


}