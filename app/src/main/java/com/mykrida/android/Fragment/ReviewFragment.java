package com.mykrida.android.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.ChangeLocationAdapter;
import com.mykrida.android.Adapter.ReviewAdapter;
import com.mykrida.android.ChangeLocationActivity;
import com.mykrida.android.R;
import com.mykrida.android.SportsDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewFragment extends Fragment {

    SportsDetailActivity thisAct;
    CardView btnWriteReview;
    RecyclerView rvReviewList;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<String> arrReview = new ArrayList<>();
    ReviewAdapter reviewAdapter;
    TextView tvNoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_review, container, false);
        btnWriteReview = v.findViewById(R.id.btnWriteReview);
        rvReviewList = v.findViewById(R.id.rvReviewList);
        tvNoDataFound = v.findViewById(R.id.tvNoDataFound);

        thisAct = (SportsDetailActivity) getActivity();
        hud = KProgressHUD.create(thisAct)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);


        btnWriteReview.setOnClickListener(view -> {
            thisAct.llReview.setVisibility(View.VISIBLE);
        });


        rvReviewList.setHasFixedSize(true);
        rvReviewList.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(thisAct, RecyclerView.VERTICAL, false);
        rvReviewList.setLayoutManager(llmF);

        GetReviewList();

        return v;
    }

    private void GetReviewList() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetReviewList(Constant.APPKEY,thisAct.trufModel.getTrufID());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrReview.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                arrReview.add(jsonObject1.getString("review"));
                            }

                            if(arrReview.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                reviewAdapter = new ReviewAdapter(thisAct, "Fragment", arrReview, "");
                                rvReviewList.setAdapter(reviewAdapter);
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(thisAct, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


}