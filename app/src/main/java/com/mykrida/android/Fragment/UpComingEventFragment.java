package com.mykrida.android.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.UpcomingEventsAdapter;
import com.mykrida.android.EventDetailsActivity;
import com.mykrida.android.Model.EventModel;
import com.mykrida.android.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpComingEventFragment extends Fragment {

    RecyclerView rvUpcomingEvent;
    UpcomingEventsAdapter upcomingEventsAdapter;
    ArrayList<EventModel> arrEvents = new ArrayList<>();
    TextView tvNoDataFound;

    ApiInterface apiService;
    KProgressHUD hud;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upcoming_events, container, false);

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvUpcomingEvent = v.findViewById(R.id.rvUpcomingEvents);
        tvNoDataFound = v.findViewById(R.id.tvNoDataFound);

        rvUpcomingEvent.setHasFixedSize(true);
        rvUpcomingEvent.setLayoutFrozen(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvUpcomingEvent.setLayoutManager(llm);

        GetEventList();

        return v;
    }

    private void GetEventList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetEventList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrEvents.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                EventModel eventModel = new EventModel();
                                eventModel.setDate(jsonObject1.getString("date"));
                                eventModel.setDescription(jsonObject1.getString("description"));
                                eventModel.setId(jsonObject1.getInt("id"));
                                eventModel.setImage(jsonObject1.getString("images"));
                                eventModel.setPlace(jsonObject1.getString("place"));
                                eventModel.setPrice(jsonObject1.getInt("ticket_price"));
                                eventModel.setTitle(jsonObject1.getString("title"));
                                eventModel.setType(jsonObject1.getInt("type"));

                                arrEvents.add(eventModel);
                            }

                            if(arrEvents.size() != 0) {
                                upcomingEventsAdapter = new UpcomingEventsAdapter(getActivity(),"",arrEvents,"");
                                rvUpcomingEvent.setAdapter(upcomingEventsAdapter);


                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        upcomingEventsAdapter.setOnItemClickListener(new UpcomingEventsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(getActivity(), EventDetailsActivity.class);
                intent.putExtra("eventdetails",arrEvents.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });


    }

}