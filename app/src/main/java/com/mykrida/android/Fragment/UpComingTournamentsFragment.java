package com.mykrida.android.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.UpcomingTournamentsAdapter;
import com.mykrida.android.Model.TournamentModel;
import com.mykrida.android.R;
import com.mykrida.android.TournamentsDetailsActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpComingTournamentsFragment extends Fragment {

    RecyclerView rvUpcomingTournaments;
    UpcomingTournamentsAdapter upcomingTournamentsAdapter;

    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<TournamentModel> arrTournaments = new ArrayList<>();
    TextView tvNoDataFound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upcoming_turnaments, container, false);

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvUpcomingTournaments = v.findViewById(R.id.rvUpcomingTurnaments);

        rvUpcomingTournaments.setHasFixedSize(true);
        rvUpcomingTournaments.setLayoutFrozen(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvUpcomingTournaments.setLayoutManager(llm);

        tvNoDataFound = v.findViewById(R.id.tvNoDataFound);

        GetTournamentList();

        return v;
    }


    private void GetTournamentList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetTournamentList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrTournaments.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                TournamentModel tournamentModel = new TournamentModel();
                                tournamentModel.setDate(jsonObject1.getString("date"));
                                tournamentModel.setDescription(jsonObject1.getString("description"));
                                tournamentModel.setId(jsonObject1.getInt("id"));
                                tournamentModel.setImage(jsonObject1.getString("images"));
                                tournamentModel.setPlace(jsonObject1.getString("place"));
                                tournamentModel.setPrice(jsonObject1.getInt("ticket_price"));
                                tournamentModel.setTitle(jsonObject1.getString("title"));
                                tournamentModel.setType(jsonObject1.getInt("type"));
                                tournamentModel.setStartTime(jsonObject1.getString("start_time"));
                                tournamentModel.setEndTime(jsonObject1.getString("end_time"));

                                arrTournaments.add(tournamentModel);
                            }

                            if(arrTournaments.size() != 0) {
                                upcomingTournamentsAdapter = new UpcomingTournamentsAdapter(getActivity(),"",arrTournaments,"");
                                rvUpcomingTournaments.setAdapter(upcomingTournamentsAdapter);

                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        upcomingTournamentsAdapter.setOnItemClickListener(new UpcomingTournamentsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(getActivity(), TournamentsDetailsActivity.class);
                intent.putExtra("tournamentModel",arrTournaments.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });


    }

}