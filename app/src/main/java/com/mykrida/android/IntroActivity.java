package com.mykrida.android;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.mykrida.android.Fragment.IntroFragment;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

public class IntroActivity extends AppCompatActivity {

    ViewPager vpIntro;
    DotsIndicator dotsIndicator;
    TextView tvButtonText;
    CardView btnNext;
    TextView btnSkip;
//    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        getSupportActionBar().hide();

        findViews();
        vpIntro.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        dotsIndicator.setViewPager(vpIntro);

        vpIntro.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    tvButtonText.setText("Get Started");
                } else {
                    tvButtonText.setText("Next");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnNext.setOnClickListener(view -> {
               if (tvButtonText.getText().toString().equals("Next")) {
                    vpIntro.setCurrentItem(vpIntro.getCurrentItem() + 1);
                } else {
                    Intent i = new Intent(IntroActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
        });

        btnSkip.setOnClickListener(view -> {
            Intent i = new Intent(IntroActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        });

    }

    private void findViews() {
        vpIntro = findViewById(R.id.vpIntro);
        dotsIndicator = findViewById(R.id.dots_indicator);
        tvButtonText = findViewById(R.id.tvButtonText);
        btnNext = findViewById(R.id.btnNext);
        btnSkip = findViewById(R.id.btnSkip);
//        videoView = findViewById(R.id.videoView);

//        String path = "android.resource://" + getPackageName() + "/" + R.raw.intro;
//        MediaController mc = new MediaController(this);
//        mc.setAnchorView(videoView);
//        mc.setMediaPlayer(videoView);
//        Uri video = Uri.parse(path);
//        videoView.setMediaController(mc);
//        videoView.setVideoURI(video);
//        videoView.start();


    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {

                case 0:
                    return IntroFragment.newInstance("Book", "Find Sports Venues, check slot availability, Book now, pay after play ", R.drawable.second);
                case 1:
                    return IntroFragment.newInstance("Find Players", "Invite Similar skill players/opponents for a game", R.drawable.first);
                case 2:
                    return IntroFragment.newInstance("Participate", "Register in sports activities", R.drawable.third);
                case 3:
                    return IntroFragment.newInstance("Learn", "Find sports academies", R.drawable.fourth);
                case 4:
                    return IntroFragment.newInstance("Learn", "Find sports academies", R.drawable.fifth);
                default:
                    return IntroFragment.newInstance("Book", "Find Sports Venues, check slot availability, Book now, pay after play ", R.drawable.map);
            }
        }

        @Override
        public int getCount() {
            return 5;
        }
    }
}