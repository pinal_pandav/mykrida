package com.mykrida.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.CommonResponse;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    CardView btnProceed, btnProceedOTP;
    EditText edtMobileNumber;
    EditText editText1, editText2, editText3, editText4, editText5, editText6;
    LinearLayout FirstLayout, SecondLayout;
    TextView tvButtonText;

    TextView ResendOTP;
    boolean Resend = false;

    KProgressHUD hud;
    ApiInterface apiService;

    SharedPreferences prefUserData;
    SharedPreferences.Editor editorUserData;


    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViews();

        edtMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() == 10) {
                    btnProceed.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    tvButtonText.setTextColor(getResources().getColor(R.color.white));
                    btnProceed.setEnabled(true);
                } else {
                    btnProceed.setCardBackgroundColor(getResources().getColor(R.color.gray));
                    tvButtonText.setTextColor(getResources().getColor(R.color.colorPrimary));
                    btnProceed.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtMobileNumber.getText().toString().trim().isEmpty()) {
                    edtMobileNumber.setError("Please enter Mobile No.");
                    edtMobileNumber.requestFocus();
                } else if (edtMobileNumber.getText().toString().trim().length() != 10) {
                    edtMobileNumber.setError("Please enter valid Mobile No.");
                    edtMobileNumber.requestFocus();
                } else {
                    if (edtMobileNumber.getText().toString().trim().equals("9988776655")) {
                        Animation animSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                        FirstLayout.startAnimation(animSlide);
                        animSlide.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                SecondLayout.setVisibility(View.VISIBLE);
                                FirstLayout.setVisibility(View.GONE);
                                editText1.requestFocus();
                                showKeyBoard(LoginActivity.this, editText1);
                                new CountDownTimer(180000, 1000) {
                                    public void onTick(long millisUntilFinished) {
                                        int seconds = (int) (millisUntilFinished / 1000);
                                        int minutes = seconds / 60;
                                        seconds = seconds % 60;
                                        ResendOTP.setText("Resend OTP in " + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                                    }

                                    public void onFinish() {
                                        Resend = true;
                                        ResendOTP.setText("Resend OTP");
                                    }
                                }.start();
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    } else {
                        sendOTPAPI();
                    }
                }
            }
        });

        btnProceedOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(LoginActivity.this);
                if (ValidateThree()) {
                    String Code = editText1.getText().toString().trim() + editText2.getText().toString().trim()
                            + editText3.getText().toString().trim() + editText4.getText().toString().trim() +
                            editText5.getText().toString().trim() + editText6.getText().toString().trim();

                    callOTPVerificationAPI(Code);
                }
            }
        });

        ResendOTP.setOnClickListener(view -> {
            if (Resend) {
                resendOTPAPI();
            }
        });

//        SmsListener smsListener = new SmsListener();
//        try {
//            unregisterReceiver(smsListener);
//        } catch (Exception e) {
//        }
//        registerReceiver(smsListener, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

    }


    public boolean ValidateThree() {
        String Code = editText1.getText().toString().trim() + editText2.getText().toString().trim()
                + editText3.getText().toString().trim() + editText4.getText().toString().trim() +
                editText5.getText().toString().trim() + editText6.getText().toString().trim();

        if (Code.length() != 6) {
            Toast.makeText(this, "Opps! OTP must be 6 character.", Toast.LENGTH_LONG).show();
            return false;
        } else {
            return true;
        }
    }

    private void callOTPVerificationAPI(String otp) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.VerifyOTP(Constant.APPKEY, edtMobileNumber.getText().toString().trim(), otp);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                try {
                    if (response.code() == 200) {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            if (!jsonObject.getString("data").equals("null")) {
                                editorUserData.putString("User_Info", jsonObject.getJSONObject("data").toString());
                                editorUserData.putBoolean("isLoggedIn", true);
                                editorUserData.apply();
                                Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivity(i);
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        editText1.setText("");
                        editText2.setText("");
                        editText3.setText("");
                        editText4.setText("");
                        editText5.setText("");
                        editText6.setText("");
                        editText1.requestFocus();
                        hideKeyboard(LoginActivity.this);
                        Toast.makeText(LoginActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void sendOTPAPI() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonResponse> callCard = apiService.SendOTP(Constant.APPKEY, edtMobileNumber.getText().toString().trim());
        callCard.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            Animation animSlide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_left);
                            FirstLayout.startAnimation(animSlide);
                            animSlide.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    SecondLayout.setVisibility(View.VISIBLE);
                                    FirstLayout.setVisibility(View.GONE);
                                    editText1.requestFocus();
                                    showKeyBoard(LoginActivity.this, editText1);
                                    new CountDownTimer(180000, 1000) {
                                        public void onTick(long millisUntilFinished) {
                                            int seconds = (int) (millisUntilFinished / 1000);
                                            int minutes = seconds / 60;
                                            seconds = seconds % 60;
                                            ResendOTP.setText("Resend OTP in " + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                                        }

                                        public void onFinish() {
                                            Resend = true;
                                            ResendOTP.setText("Resend OTP");
                                        }
                                    }.start();
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }
                            });
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Server Error!", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }


    private void resendOTPAPI() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<CommonResponse> callCard = apiService.resendOTP(Constant.APPKEY, edtMobileNumber.getText().toString().trim());
        callCard.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            Toast.makeText(LoginActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                            Resend = false;
                            new CountDownTimer(180000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    int seconds = (int) (millisUntilFinished / 1000);
                                    int minutes = seconds / 60;
                                    seconds = seconds % 60;
                                    ResendOTP.setText("Resend OTP in " + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                                }

                                public void onFinish() {
                                    Resend = true;
                                    ResendOTP.setText("Resend OTP");
                                }
                            }.start();
                        } else {
                            Toast.makeText(LoginActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    public void showKeyBoard(Activity activity, EditText editText) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    private void findViews() {
        btnProceed = findViewById(R.id.btnProceed);
        btnProceed.setEnabled(false);
        btnProceedOTP = findViewById(R.id.btnProceedOTP);
        edtMobileNumber = findViewById(R.id.edtMobileNumber);

        tvButtonText = findViewById(R.id.tvButtonText);

        editText1 = findViewById(R.id.edittext1);
        editText2 = findViewById(R.id.edittext2);
        editText3 = findViewById(R.id.edittext3);
        editText4 = findViewById(R.id.edittext4);
        editText5 = findViewById(R.id.edittext5);
        editText6 = findViewById(R.id.edittext6);

        editText1.addTextChangedListener(new GenericTextWatcher(editText1));
        editText2.addTextChangedListener(new GenericTextWatcher(editText2));
        editText3.addTextChangedListener(new GenericTextWatcher(editText3));
        editText4.addTextChangedListener(new GenericTextWatcher(editText4));
        editText5.addTextChangedListener(new GenericTextWatcher(editText5));
        editText6.addTextChangedListener(new GenericTextWatcher(editText6));

        FirstLayout = findViewById(R.id.FirstPart);
        SecondLayout = findViewById(R.id.SecondPart);
        ResendOTP = findViewById(R.id.ResendOTP);

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        editorUserData = prefUserData.edit();
    }

    public class GenericTextWatcher implements TextWatcher {
        private final View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.edittext1:
                    if (text.length() == 1)
                        editText2.requestFocus();
                    break;
                case R.id.edittext2:
                    if (text.length() == 1)
                        editText3.requestFocus();
                    else
                        editText1.requestFocus();
                    break;
                case R.id.edittext3:
                    if (text.length() == 1)
                        editText4.requestFocus();
                    else
                        editText2.requestFocus();
                    break;
                case R.id.edittext4:
                    if (text.length() == 1)
                        editText5.requestFocus();
                    else
                        editText3.requestFocus();
                    break;
                case R.id.edittext5:
                    if (text.length() == 1)
                        editText6.requestFocus();
                    else
                        editText4.requestFocus();
                    break;
                case R.id.edittext6:
                    if (text.length() == 0)
                        editText5.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

}