package com.mykrida.android.Model.AtheleData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Athele {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_no")
    @Expose
    private String mobile_no;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profile_pic")
    @Expose
    private String profile_pic;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("time_hours")
    @Expose
    private String time_hours;
    @SerializedName("languages")
    @Expose
    private String languages;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("sports")
    @Expose
    private String sports;
    @SerializedName("pricing")
    @Expose
    private String pricing;
    @SerializedName("event_prefered")
    @Expose
    private String event_prefered;
    @SerializedName("page_title")
    @Expose
    private String page_title;
    @SerializedName("slug")
    @Expose
    private String slug;



}
