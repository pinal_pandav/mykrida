package com.mykrida.android.Model.AtheleData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class AtheleResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<Athele> data;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
}
