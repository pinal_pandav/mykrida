package com.mykrida.android.Model.AtheleDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class AtheleDetailsResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<AtheleDetails> data;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("gallery-images")
    @Expose
    private ArrayList<Images> galleryImages;
}
