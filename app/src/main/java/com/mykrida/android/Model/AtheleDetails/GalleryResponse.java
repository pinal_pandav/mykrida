package com.mykrida.android.Model.AtheleDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class GalleryResponse {
    @SerializedName("images")
    @Expose
    private ArrayList<Images> images;
}
