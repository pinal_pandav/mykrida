package com.mykrida.android.Model.AtheleDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import lombok.Data;

@Data
public class Images {
    @SerializedName("image")
    @Expose
    private String image;
    private int drawableImage;
}
