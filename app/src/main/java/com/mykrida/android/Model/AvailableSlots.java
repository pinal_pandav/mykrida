package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class AvailableSlots implements Parcelable {
    String from_time,to_time;
    int time_id,truf_id,price;
    boolean isChecked;


    protected AvailableSlots(Parcel in) {
        from_time = in.readString();
        to_time = in.readString();
        time_id = in.readInt();
        truf_id = in.readInt();
        price = in.readInt();
        isChecked = in.readByte() != 0;
    }

    public static final Creator<AvailableSlots> CREATOR = new Creator<AvailableSlots>() {
        @Override
        public AvailableSlots createFromParcel(Parcel in) {
            return new AvailableSlots(in);
        }

        @Override
        public AvailableSlots[] newArray(int size) {
            return new AvailableSlots[size];
        }
    };

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public AvailableSlots() {
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFrom_time() {
        return from_time;
    }

    public void setFrom_time(String from_time) {
        this.from_time = from_time;
    }

    public String getTo_time() {
        return to_time;
    }

    public void setTo_time(String to_time) {
        this.to_time = to_time;
    }

    public int getTime_id() {
        return time_id;
    }

    public void setTime_id(int time_id) {
        this.time_id = time_id;
    }

    public int getTruf_id() {
        return truf_id;
    }

    public void setTruf_id(int truf_id) {
        this.truf_id = truf_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(from_time);
        parcel.writeString(to_time);
        parcel.writeInt(time_id);
        parcel.writeInt(truf_id);
        parcel.writeInt(price);
        parcel.writeByte((byte) (isChecked ? 1 : 0));
    }
}
