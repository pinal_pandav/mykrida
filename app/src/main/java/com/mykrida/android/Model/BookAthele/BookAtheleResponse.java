package com.mykrida.android.Model.BookAthele;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mykrida.android.Model.AtheleDetails.AtheleDetails;

import java.util.ArrayList;

import lombok.Data;

@Data
public class BookAtheleResponse {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
}
