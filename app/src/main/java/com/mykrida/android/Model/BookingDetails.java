package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class BookingDetails implements Parcelable {

    int id,uid,trufid,time_id,cid,vid,scid,pincode,rate,booking_id;
    String booking_status;
    String slotTime,price;
    String date,trans_id,payment_status,booking_name,address,city,state,coupon,tname,images,description,pricing_set;
    public BookingDetails() {
    }

    protected BookingDetails(Parcel in) {
        id = in.readInt();
        uid = in.readInt();
        trufid = in.readInt();
        time_id = in.readInt();
        cid = in.readInt();
        vid = in.readInt();
        scid = in.readInt();
        price = in.readString();
        pincode = in.readInt();
        rate = in.readInt();
        date = in.readString();
        trans_id = in.readString();
        payment_status = in.readString();
        booking_name = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        coupon = in.readString();
        tname = in.readString();
        images = in.readString();
        description = in.readString();
        pricing_set = in.readString();
        booking_id = in.readInt();
        booking_status = in.readString();
        slotTime = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(uid);
        dest.writeInt(trufid);
        dest.writeInt(time_id);
        dest.writeInt(cid);
        dest.writeInt(vid);
        dest.writeInt(scid);
        dest.writeString(price);
        dest.writeInt(pincode);
        dest.writeInt(rate);
        dest.writeString(date);
        dest.writeString(trans_id);
        dest.writeString(payment_status);
        dest.writeString(booking_name);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(coupon);
        dest.writeString(tname);
        dest.writeString(images);
        dest.writeString(description);
        dest.writeString(pricing_set);
        dest.writeInt(booking_id);
        dest.writeString(booking_status);
        dest.writeString(slotTime);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingDetails> CREATOR = new Creator<BookingDetails>() {
        @Override
        public BookingDetails createFromParcel(Parcel in) {
            return new BookingDetails(in);
        }

        @Override
        public BookingDetails[] newArray(int size) {
            return new BookingDetails[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getTrufid() {
        return trufid;
    }

    public void setTrufid(int trufid) {
        this.trufid = trufid;
    }

    public int getTime_id() {
        return time_id;
    }

    public void setTime_id(int time_id) {
        this.time_id = time_id;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }

    public int getScid() {
        return scid;
    }

    public void setScid(int scid) {
        this.scid = scid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getPincode() {
        return pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getBooking_name() {
        return booking_name;
    }

    public void setBooking_name(String booking_name) {
        this.booking_name = booking_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPricing_set() {
        return pricing_set;
    }

    public void setPricing_set(String pricing_set) {
        this.pricing_set = pricing_set;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(int booking_id) {
        this.booking_id = booking_id;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(String slotTime) {
        this.slotTime = slotTime;
    }
}
