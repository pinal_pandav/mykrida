package com.mykrida.android.Model;

public class CategoryList {

    String cName,cDate;
    int id,isDeleted;

    public CategoryList(String cName, String cDate, int id, int isDeleted) {
        this.cName = cName;
        this.cDate = cDate;
        this.id = id;
        this.isDeleted = isDeleted;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getcDate() {
        return cDate;
    }

    public void setcDate(String cDate) {
        this.cDate = cDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(int isDeleted) {
        this.isDeleted = isDeleted;
    }
}
