package com.mykrida.android.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mykrida.android.Model.wallet_amount.WalletData;

import lombok.Data;

@Data
public class CommonResponse {
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
}
