package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class CouponModel implements Parcelable {
    int id,discount;
    String coupon_code,image;
    String start_date,end_date;

    public CouponModel() {
    }

    protected CouponModel(Parcel in) {
        id = in.readInt();
        discount = in.readInt();
        coupon_code = in.readString();
        image = in.readString();
        start_date = in.readString();
        end_date = in.readString();
    }

    public static final Creator<CouponModel> CREATOR = new Creator<CouponModel>() {
        @Override
        public CouponModel createFromParcel(Parcel in) {
            return new CouponModel(in);
        }

        @Override
        public CouponModel[] newArray(int size) {
            return new CouponModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(discount);
        parcel.writeString(coupon_code);
        parcel.writeString(image);
        parcel.writeString(start_date);
        parcel.writeString(end_date);
    }
}
