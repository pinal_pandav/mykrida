package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class EventDetailsModel implements Parcelable {

    int id,eid,uid,no_of_ticket,tot_amount,type;
    String booking_date,images,title,description,date,place,ticket_price;

    public EventDetailsModel() {
    }

    protected EventDetailsModel(Parcel in) {
        id = in.readInt();
        eid = in.readInt();
        uid = in.readInt();
        no_of_ticket = in.readInt();
        tot_amount = in.readInt();
        type = in.readInt();
        booking_date = in.readString();
        images = in.readString();
        title = in.readString();
        description = in.readString();
        date = in.readString();
        place = in.readString();
        ticket_price = in.readString();
    }

    public static final Creator<EventDetailsModel> CREATOR = new Creator<EventDetailsModel>() {
        @Override
        public EventDetailsModel createFromParcel(Parcel in) {
            return new EventDetailsModel(in);
        }

        @Override
        public EventDetailsModel[] newArray(int size) {
            return new EventDetailsModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getNo_of_ticket() {
        return no_of_ticket;
    }

    public void setNo_of_ticket(int no_of_ticket) {
        this.no_of_ticket = no_of_ticket;
    }

    public int getTot_amount() {
        return tot_amount;
    }

    public void setTot_amount(int tot_amount) {
        this.tot_amount = tot_amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getTicket_price() {
        return ticket_price;
    }

    public void setTicket_price(String ticket_price) {
        this.ticket_price = ticket_price;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(eid);
        dest.writeInt(uid);
        dest.writeInt(no_of_ticket);
        dest.writeInt(tot_amount);
        dest.writeInt(type);
        dest.writeString(booking_date);
        dest.writeString(images);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(date);
        dest.writeString(place);
        dest.writeString(ticket_price);
    }
}
