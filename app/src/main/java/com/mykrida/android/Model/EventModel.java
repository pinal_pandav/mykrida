package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class EventModel implements Parcelable {

    int id,type,price;
    String image,title,description,date,place;

    public EventModel() {
    }

    protected EventModel(Parcel in) {
        id = in.readInt();
        type = in.readInt();
        price = in.readInt();
        image = in.readString();
        title = in.readString();
        description = in.readString();
        date = in.readString();
        place = in.readString();
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(type);
        dest.writeInt(price);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(date);
        dest.writeString(place);
    }
}
