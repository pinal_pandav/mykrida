package com.mykrida.android.Model;

public class SportsModel {

    String tname,image;

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public SportsModel() {
    }
}
