package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class TournamentModel implements Parcelable {

    int id,type,price;
    String image,title,description,date,place;
    String startTime,endTime;

    public TournamentModel() {
    }

    protected TournamentModel(Parcel in) {
        id = in.readInt();
        type = in.readInt();
        price = in.readInt();
        image = in.readString();
        title = in.readString();
        description = in.readString();
        date = in.readString();
        place = in.readString();
        startTime = in.readString();
        endTime = in.readString();
    }

    public static final Creator<TournamentModel> CREATOR = new Creator<TournamentModel>() {
        @Override
        public TournamentModel createFromParcel(Parcel in) {
            return new TournamentModel(in);
        }

        @Override
        public TournamentModel[] newArray(int size) {
            return new TournamentModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(type);
        parcel.writeInt(price);
        parcel.writeString(image);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(date);
        parcel.writeString(place);
        parcel.writeString(startTime);
        parcel.writeString(endTime);
    }
}
