package com.mykrida.android.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class TrufModel implements Parcelable {
    int id,vid,cid,scid;
    String tname,price,address,description,procingset,fromtime,totime,day;
    int trufID;
    String images;
    int rating;


    protected TrufModel(Parcel in) {
        id = in.readInt();
        vid = in.readInt();
        price = in.readString();
        cid = in.readInt();
        scid = in.readInt();
        tname = in.readString();
        address = in.readString();
        description = in.readString();
        procingset = in.readString();
        fromtime = in.readString();
        totime = in.readString();
        day = in.readString();
        trufID = in.readInt();
        images = in.readString();
        rating = in.readInt();
    }

    public static final Creator<TrufModel> CREATOR = new Creator<TrufModel>() {
        @Override
        public TrufModel createFromParcel(Parcel in) {
            return new TrufModel(in);
        }

        @Override
        public TrufModel[] newArray(int size) {
            return new TrufModel[size];
        }
    };

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getTrufID() {
        return trufID;
    }

    public void setTrufID(int trufID) {
        this.trufID = trufID;
    }

    public TrufModel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVid() {
        return vid;
    }

    public void setVid(int vid) {
        this.vid = vid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getScid() {
        return scid;
    }

    public void setScid(int scid) {
        this.scid = scid;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProcingset() {
        return procingset;
    }

    public void setProcingset(String procingset) {
        this.procingset = procingset;
    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(vid);
        parcel.writeString(price);
        parcel.writeInt(cid);
        parcel.writeInt(scid);
        parcel.writeString(tname);
        parcel.writeString(address);
        parcel.writeString(description);
        parcel.writeString(procingset);
        parcel.writeString(fromtime);
        parcel.writeString(totime);
        parcel.writeString(day);
        parcel.writeInt(trufID);
        parcel.writeString(images);
        parcel.writeInt(rating);
    }
}
