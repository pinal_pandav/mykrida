package com.mykrida.android.Model.tournaments;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;

@Data
public class Tournaments implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("booking_date")
    @Expose
    private String booking_date;
    @SerializedName("no_of_ticket")
    @Expose
    private int no_of_ticket;
    @SerializedName("total_amount")
    @Expose
    private int total_amount;
    @SerializedName("place")
    @Expose
    private String place;
}
