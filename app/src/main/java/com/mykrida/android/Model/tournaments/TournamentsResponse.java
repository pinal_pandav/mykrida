package com.mykrida.android.Model.tournaments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mykrida.android.Model.AtheleData.Athele;

import java.util.ArrayList;

import lombok.Data;

@Data
public class TournamentsResponse {
    @SerializedName("data")
    @Expose
    private ArrayList<Tournaments> data;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("msg")
    @Expose
    private String msg;
}
