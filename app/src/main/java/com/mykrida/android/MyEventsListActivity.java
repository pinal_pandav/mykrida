package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.EventDetailsAdapter;
import com.mykrida.android.Model.EventDetailsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyEventsListActivity extends AppCompatActivity {

    RecyclerView rvBookingDetails;
    EventDetailsAdapter eventDetailsAdapter;
    ImageView btnBack;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<EventDetailsModel> eventDetailsModelArrayList = new ArrayList<>();
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    TextView tvNoDataFound;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews();

        tvTitle.setText("My Events");

        hud = KProgressHUD.create(MyEventsListActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvBookingDetails.setHasFixedSize(true);
        rvBookingDetails.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvBookingDetails.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(MyEventsListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(MyEventsListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        GetBookingDetails();

    }


    private void findViews() {
        rvBookingDetails = findViewById(R.id.rvBookingDetails);
        btnBack = findViewById(R.id.btnBack);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
        tvTitle = findViewById(R.id.tvTitle);
    }

    private void GetBookingDetails() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetMyEventsList(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        eventDetailsModelArrayList.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                EventDetailsModel eventDetailsModel = new EventDetailsModel();
                                eventDetailsModel.setId(jsonObject1.getInt("id"));
                                eventDetailsModel.setEid(jsonObject1.getInt("eid"));
                                eventDetailsModel.setUid(jsonObject1.getInt("uid"));
                                eventDetailsModel.setNo_of_ticket(jsonObject1.getInt("no_of_ticket"));
                                eventDetailsModel.setTot_amount(jsonObject1.getInt("tot_amount"));
                                eventDetailsModel.setBooking_date(jsonObject1.getString("booking_date"));
                                eventDetailsModel.setImages(jsonObject1.getString("images"));
                                eventDetailsModel.setTitle(jsonObject1.getString("title"));
                                eventDetailsModel.setDescription(jsonObject1.getString("description"));
                                eventDetailsModel.setType(jsonObject1.getInt("type"));
                                eventDetailsModel.setDate(jsonObject1.getString("date"));
                                eventDetailsModel.setPlace(jsonObject1.getString("place"));
                                eventDetailsModel.setTicket_price(jsonObject1.getString("ticket_price"));


                                eventDetailsModelArrayList.add(eventDetailsModel);
                            }

                            if(eventDetailsModelArrayList.size() != 0) {
                                tvNoDataFound.setVisibility(View.GONE);
                                eventDetailsAdapter = new EventDetailsAdapter(MyEventsListActivity.this, "Fragment", eventDetailsModelArrayList, "");
                                rvBookingDetails.setAdapter(eventDetailsAdapter);
                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(MyEventsListActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        eventDetailsAdapter.setOnItemClickListener(new EventDetailsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(MyEventsListActivity.this,EventDetailsShowActivity.class);
                intent.putExtra("eventdetails",eventDetailsModelArrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });


    }


}