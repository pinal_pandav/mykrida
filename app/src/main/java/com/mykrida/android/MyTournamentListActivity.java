package com.mykrida.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.EventDetailsAdapter;
import com.mykrida.android.Adapter.TournamentDetailsAdapter;
import com.mykrida.android.Model.EventDetailsModel;
import com.mykrida.android.Model.tournaments.Tournaments;
import com.mykrida.android.Model.tournaments.TournamentsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTournamentListActivity extends AppCompatActivity {

    RecyclerView rvBookingDetails;
    TournamentDetailsAdapter tournamentDetailsAdapter;
    ImageView btnBack;
    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ArrayList<Tournaments> eventDetailsModelArrayList = new ArrayList<>();
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    TextView tvNoDataFound;
    TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_list);

        getSupportActionBar().hide();

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        findViews();

        tvTitle.setText("My Tournaments");

        hud = KProgressHUD.create(MyTournamentListActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvBookingDetails.setHasFixedSize(true);
        rvBookingDetails.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvBookingDetails.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(MyTournamentListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(MyTournamentListActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        getTournaments();

    }


    private void findViews() {
        rvBookingDetails = findViewById(R.id.rvBookingDetails);
        btnBack = findViewById(R.id.btnBack);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
        tvTitle = findViewById(R.id.tvTitle);
    }

    private void getTournaments() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TournamentsResponse> callCard = apiService.getTournaments(Constant.APPKEY,jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<TournamentsResponse>() {
            @Override
            public void onResponse(Call<TournamentsResponse> call, Response<TournamentsResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        eventDetailsModelArrayList.addAll(response.body().getData());
                        if(eventDetailsModelArrayList.size() != 0) {
                            tvNoDataFound.setVisibility(View.GONE);
                            tournamentDetailsAdapter = new TournamentDetailsAdapter(MyTournamentListActivity.this, "Fragment", eventDetailsModelArrayList, "");
                            rvBookingDetails.setAdapter(tournamentDetailsAdapter);
                            setAdapterClick();
                        }else{
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure (Call <TournamentsResponse> call, Throwable t){
                hud.dismiss();
            }
        });
    }
    private void setAdapterClick() {

        tournamentDetailsAdapter.setOnItemClickListener(new TournamentDetailsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(MyTournamentListActivity.this,TournamentsDetailsShowActivity.class);
                intent.putExtra("details",eventDetailsModelArrayList.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {
            }
        });


    }


}