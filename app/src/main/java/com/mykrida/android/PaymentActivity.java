package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Model.AvailableSlots;
import com.mykrida.android.Model.CouponModel;
import com.mykrida.android.Model.TrufModel;
import com.mykrida.android.Model.wallet_amount.UpdateWalletAmountResponse;
import com.mykrida.android.Model.wallet_amount.WalletAmountResponse;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONException;
import org.json.JSONObject;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends Activity implements PaymentResultListener {

    ImageView btnBack;
    TextView tvPrice, tvNetPayable;
    CardView btnProceedToPayment;

    ApiInterface apiService;
    KProgressHUD hud;
    AvailableSlots availableSlots;
    TrufModel trufModel;
    String date;
    SharedPreferences prefUserData;
    SharedPreferences.Editor editorUserData;
    JsonObject jobjUser;
    SmoothBottomBar smoothBottomBar;
    String transactionID = "", paymentStatus = "", coupon = "";

    EditText edtPersonName, edtAddress, edtCity, edtState, edtPincode;
    String Name = "";
    String Email = "test@gmail.com";
    String PhoneNumber = "9988556699";
    CouponModel couponModel = null;
    TextView tvCoupon, tvDiscountPrice;
    LinearLayout llCoupon;
    float TotalPayableAmount = 0;
    float PayableAmount = 0;
    String BookName;
    String Address;
    String City;
    String State;
    String Pincode;
    boolean updateWalletNeeded = false;

    CheckBox cbWallet;
    TextView tvWalletAmount;
    int walletAmount = 0;
    float updatedwalletAmount = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        editorUserData = prefUserData.edit();
        String UserInfo = prefUserData.getString("User_Info", "");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);

        hud = KProgressHUD.create(PaymentActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        findViews();

        if(jobjUser.has("name") && !jobjUser.get("name").isJsonNull()){
            edtPersonName.setText(jobjUser.get("name").getAsString());
        }
        if(jobjUser.has("city") && !jobjUser.get("city").isJsonNull()){
            edtCity.setText(jobjUser.get("city").getAsString());
        }
        if(jobjUser.has("address") && !jobjUser.get("address").isJsonNull()){
            edtAddress.setText(jobjUser.get("address").getAsString());
        }


        Intent i = getIntent();
        availableSlots = i.getExtras().getParcelable("data");
        trufModel = i.getExtras().getParcelable("trufdata");
        if (i.hasExtra("coupon")) {
            couponModel = i.getExtras().getParcelable("coupon");
        }
        tvPrice.setText("\u20B9 " + String.valueOf(availableSlots.getPrice()));

        date = i.getStringExtra("date");

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        GetWallet();

        btnProceedToPayment.setOnClickListener(view -> {
            if (Validate()) {

                if (couponModel != null) {
                    tvCoupon.setText("Coupon (" + couponModel.getDiscount() + "%)");
                    float discountPrice = (availableSlots.getPrice() * couponModel.getDiscount()) / 100;
                    tvDiscountPrice.setText("\u20B9 " + String.valueOf(discountPrice));
                    TotalPayableAmount = availableSlots.getPrice() - discountPrice;
                    tvNetPayable.setText("\u20B9 " + String.valueOf(TotalPayableAmount));
                    coupon = couponModel.getCoupon_code();
                } else {
                    llCoupon.setVisibility(View.GONE);
                    tvNetPayable.setText("\u20B9 " + String.valueOf(availableSlots.getPrice()));
                    TotalPayableAmount = availableSlots.getPrice();
                }
                BookName = edtPersonName.getText().toString().trim();
                Address = edtAddress.getText().toString().trim();
                City = edtCity.getText().toString().trim();
                State = edtState.getText().toString().trim();
                Pincode = edtPincode.getText().toString().trim();

//                    if(jobjUser.get("email").isJsonNull()){
//                        Toast.makeText(PaymentActivity.this, "Please add Email Address in Profile", Toast.LENGTH_SHORT).show();
//                    }else{
                Name = BookName;
                if(cbWallet.isChecked()){
                    updateWalletNeeded = true;
                    float newWalletAmount = walletAmount - TotalPayableAmount;
                    if(newWalletAmount <=0){
                        PayableAmount = Math.abs(newWalletAmount);
                        updatedwalletAmount = walletAmount;
                    }else{
                        updatedwalletAmount = TotalPayableAmount;
                    }
                }else{
                    PayableAmount = TotalPayableAmount;
                    updateWalletNeeded = false;
                }
                if(PayableAmount > 0) {
                    startPayment();
                }else{
                    transactionID = "fromwallet";
                    paymentStatus = "success";
                    BookTruf(BookName, Address, City, State, Pincode);
                }
            }

        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        if (couponModel != null) {
            tvCoupon.setText("Coupon (" + couponModel.getDiscount() + "%)");
            float discountPrice = (availableSlots.getPrice() * couponModel.getDiscount()) / 100;
            tvDiscountPrice.setText("\u20B9 " + String.valueOf(discountPrice));
            TotalPayableAmount = availableSlots.getPrice() - discountPrice;
            tvNetPayable.setText("\u20B9 " + String.valueOf(TotalPayableAmount));
            coupon = couponModel.getCoupon_code();
        } else {
            llCoupon.setVisibility(View.GONE);
            tvNetPayable.setText("\u20B9 " + String.valueOf(availableSlots.getPrice()));
            TotalPayableAmount = availableSlots.getPrice();
        }

        if (!jobjUser.get("email").isJsonNull()) {
            Email = jobjUser.get("email").getAsString();
        }
        if (!jobjUser.get("phone_no").isJsonNull()) {
            PhoneNumber = jobjUser.get("phone_no").getAsString();
        }

    }

    private boolean Validate() {
        String personName = edtPersonName.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String city = edtCity.getText().toString().trim();
        String state = edtState.getText().toString().trim();
        String pincode = edtPincode.getText().toString().trim();
        if (personName.isEmpty()) {
            edtPersonName.setError("Please enter Person Name");
            return false;
        } else if (address.isEmpty()) {
            edtAddress.setError("Please enter Address");
            return false;
        } else if (city.isEmpty()) {
            edtCity.setError("Please enter city");
            return false;
        } else if (state.isEmpty()) {
            edtState.setError("Please enter State");
            return false;
        } else if (pincode.isEmpty()) {
            edtPincode.setError("Please enter Pincode");
            return false;
        } else {
            return true;
        }
    }

    private void findViews() {
        tvPrice = findViewById(R.id.tvPrice);
        tvNetPayable = findViewById(R.id.tvNetPayable);
        btnProceedToPayment = findViewById(R.id.btnProceedToPayment);
        edtPersonName = findViewById(R.id.edtBookingName);
        edtAddress = findViewById(R.id.edtAddress);
        edtCity = findViewById(R.id.edtCity);
        edtPincode = findViewById(R.id.edtPincode);
        edtState = findViewById(R.id.edtState);
        tvCoupon = findViewById(R.id.tvCoupon);
        tvDiscountPrice = findViewById(R.id.tvDiscountPrice);
        llCoupon = findViewById(R.id.llCoupon);
        cbWallet = findViewById(R.id.cbUseWallet);
        tvWalletAmount = findViewById(R.id.tvWalletAmount);
    }

    private void BookTruf(String personName, String address, String city, String state, String pincode) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.BookTruf(Constant.APPKEY,
                trufModel.getId(),
                availableSlots.getTime_id(),
                date,
                availableSlots.getPrice(),
                jobjUser.get("id").getAsInt(),
                transactionID,
                paymentStatus,
                personName,
                address,
                city,
                state,
                pincode,
                coupon);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        Toast.makeText(PaymentActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        if (jsonObject.getString("status").equals("1")) {
                            if(updateWalletNeeded){
                                updateWallet();
                            }else {
                                Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                                intent.putExtra("position", 0);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }



    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();
        co.setKeyID(getString(R.string.razorpaykey));

        try {
            JSONObject options = new JSONObject();
            options.put("name", "MyKrida");
            options.put("description", "MyKrida");
            options.put("send_sms_hash", true);
            options.put("allow_rotation", true);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", String.format("%.2f", Double.parseDouble(String.valueOf(PayableAmount)) * 100));

            JSONObject preFill = new JSONObject();
            preFill.put("email", Email);
            preFill.put("contact", PhoneNumber);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Toast.makeText(this, "Payment Successful ", Toast.LENGTH_SHORT).show();
            transactionID = razorpayPaymentID;
            paymentStatus = "success";
            BookTruf(BookName, Address, City, State, Pincode);

        } catch (Exception e) {
            Log.e("TAG", "Exception in onPaymentSuccess", e);
        }
    }

    /**
     * The name of the function has to be
     * onPaymentError
     * Wrap your code in try catch, as shown, to ensure that this method runs correctly
     */
    @Override
    public void onPaymentError(int code, String response) {
        Log.d("Error_payment_id", response);
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject) jsonParser.parse(response);
        if (jo.has("error")) {
            Toast.makeText(this, "" + jo.get("error").getAsJsonObject().get("description").getAsString(), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "" + jo.get("description").getAsString(), Toast.LENGTH_SHORT).show();
        }
    }

    private void GetWallet() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<WalletAmountResponse> callCard = apiService.getWalletAmount(Constant.APPKEY, jobjUser.get("id").getAsInt());
        callCard.enqueue(new Callback<WalletAmountResponse>() {
            @Override
            public void onResponse(Call<WalletAmountResponse> call, Response<WalletAmountResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus() == 1) {
                        if(response.body().getData() != null) {
                            tvWalletAmount.setText("\u20B9 " +response.body().getData().getAmount());
                            walletAmount = Integer.parseInt(response.body().getData().getAmount());
                        }else{
                            tvWalletAmount.setText("\u20B9 " +"0");
                            walletAmount = 0;
                        }
                        if(walletAmount <=0){
                            cbWallet.setChecked(false);
                            cbWallet.setEnabled(false);
                        }else{
                            cbWallet.setChecked(true);
                            cbWallet.setEnabled(true);
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<WalletAmountResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void updateWallet() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<UpdateWalletAmountResponse> callCard = apiService.updateWalletAmount(Constant.APPKEY, jobjUser.get("id").getAsInt(), String.valueOf(updatedwalletAmount)
                );
        callCard.enqueue(new Callback<UpdateWalletAmountResponse>() {
            @Override
            public void onResponse(Call<UpdateWalletAmountResponse> call, Response<UpdateWalletAmountResponse> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    assert response.body() != null;
                    if (response.body().getStatus() == 1) {
                        Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                        intent.putExtra("position", 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }

                }
            }

            @Override
            public void onFailure(Call<UpdateWalletAmountResponse> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

}