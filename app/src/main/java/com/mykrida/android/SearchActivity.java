package com.mykrida.android;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.CenterMenuAdapter;
import com.mykrida.android.Adapter.SearchAdapter;
import com.mykrida.android.Model.TrufModel;
import com.skydoves.powermenu.CustomPowerMenu;
import com.skydoves.powermenu.OnMenuItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    RecyclerView rvSearch;
    SearchAdapter searchAdapter;
    ImageView btnBack;
    ImageView btnSort, btnFilter;
    private CustomPowerMenu<String, CenterMenuAdapter> menuSortBy;
    RelativeLayout rlMainLayout;
    String Date;
    int CategoryID;
    KProgressHUD hud;
    ApiInterface apiService;
    TextView tvNoDataFound;
    ArrayList<TrufModel> trufModelArrayList = new ArrayList<>();
    TextView tvDate, tvCategory;
    SmoothBottomBar smoothBottomBar;
    SharedPreferences prefUserData;
    String Location;
    TextView tvDateSelect;
    LinearLayout rlSelectDate;
    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().hide();


        prefUserData = getSharedPreferences("USERDATA", MODE_PRIVATE);
        Location = prefUserData.getString("location", "N/A");

        findViews();

        tvDateSelect.setOnClickListener(view -> {

            DatePickerDialog datePicker = new DatePickerDialog(SearchActivity.this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePicker.show();
        });

        rvSearch.setHasFixedSize(true);
        rvSearch.setLayoutFrozen(true);
        LinearLayoutManager llmF = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvSearch.setLayoutManager(llmF);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(SearchActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(SearchActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });


        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });
        menuSortBy = PowerMenuUtils.getSortPowerMenu(SearchActivity.this, this, onWriteItemClickListener);

        btnSort.setOnClickListener(view -> {
            if (menuSortBy.isShowing()) {
                menuSortBy.dismiss();
                return;
            }
            menuSortBy.showAtCenter(rlMainLayout);
        });

        btnFilter.setOnClickListener(view -> {
            Intent i = new Intent(SearchActivity.this, FilterActivity.class);
            startActivity(i);
        });

    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateLabel();
        }

    };

    private final OnMenuItemClickListener<String> onWriteItemClickListener =
            new OnMenuItemClickListener<String>() {
                @Override
                public void onItemClick(int position, String title) {
                    Toast.makeText(SearchActivity.this, title, Toast.LENGTH_SHORT).show();
                    menuSortBy.dismiss();
                }
            };

    private void updateLabel() {
        String myFormat = "dd MMM yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDateSelect.setText(sdf.format(myCalendar.getTime()));
        tvDate.setText(sdf.format(myCalendar.getTime()));
        String myFormatforAPI = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdfforAPI = new SimpleDateFormat(myFormatforAPI, Locale.US);
        Date = sdfforAPI.format(myCalendar.getTime());

        GetTrufData(Date, CategoryID);

    }

    boolean isFromSearch;
    private void findViews() {
        rvSearch = findViewById(R.id.rvSearch);
        btnBack = findViewById(R.id.btnBack);
        btnSort = findViewById(R.id.btnSort);
        btnFilter = findViewById(R.id.btnFilter);
        rlMainLayout = findViewById(R.id.rlMainLayout);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);
        tvCategory = findViewById(R.id.tvCategory);
        tvDate = findViewById(R.id.tvDate);

        tvDateSelect = findViewById(R.id.tvDateSelect);
        rlSelectDate = findViewById(R.id.rlSelectDate);

        Intent i = getIntent();
        Date = i.getStringExtra("date");
        CategoryID = i.getIntExtra("id", 0);
        tvCategory.setText(i.getStringExtra("catName"));
        tvDate.setText(i.getStringExtra("strDate"));
        isFromSearch = i.getBooleanExtra("isFromSearch",false);
        tvDateSelect.setText(i.getStringExtra("strDate"));

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        if (Location.equals("N/A")) {
            finish();
            Toast.makeText(this, "Please select Location from Profile", Toast.LENGTH_SHORT).show();
        } else {
            if(isFromSearch) {
                rlSelectDate.setVisibility(View.GONE);
                GetTrufData(Date, CategoryID);
            }else{
                rlSelectDate.setVisibility(View.VISIBLE);
                GetTrufData(Date, CategoryID);
            }
        }


    }

    private void GetTrufData(String Date, int CategoryID) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetTrufList(Constant.APPKEY, CategoryID, Date, Location);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                trufModelArrayList.clear();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                TrufModel trufModel = new TrufModel();
                                trufModel.setId(jsonObject1.getInt("id"));
                                trufModel.setCid(jsonObject1.getInt("cid"));
                                trufModel.setScid(jsonObject1.getInt("scid"));
                                trufModel.setVid(jsonObject1.getInt("vid"));
                                trufModel.setTname(jsonObject1.getString("tname"));
                                trufModel.setAddress(jsonObject1.getString("address"));
                                trufModel.setDescription(jsonObject1.getString("descr"));
                                trufModel.setProcingset(jsonObject1.getString("pricing_set"));
                                trufModel.setPrice(jsonObject1.getString("turf_price"));
                                trufModel.setDay(jsonObject1.getString("day"));
                                trufModel.setFromtime(jsonObject1.getString("from_time"));
                                trufModel.setTotime(jsonObject1.getString("to_time"));
                                trufModel.setImages(jsonObject1.getString("images"));
                                trufModel.setTrufID(jsonObject1.getInt("turf_id"));
                                trufModel.setRating(jsonObject1.getInt("rate"));
                                trufModelArrayList.add(trufModel);

                            }
                            if (trufModelArrayList.size() != 0) {
                                searchAdapter = new SearchAdapter(SearchActivity.this, "Fragment", trufModelArrayList, "");
                                rvSearch.setAdapter(searchAdapter);
                                setClickEventOfListView();
                                rvSearch.setVisibility(View.VISIBLE);
                                tvNoDataFound.setVisibility(View.GONE);
                            } else {
                                rvSearch.setVisibility(View.GONE);
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            rvSearch.setVisibility(View.GONE);
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    rvSearch.setVisibility(View.GONE);
                    tvNoDataFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void GetTrufDataSports(String Date, int CategoryID) {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetTrufList(Constant.APPKEY, CategoryID, Date, Location);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.has("data")) {
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                TrufModel trufModel = new TrufModel();
                                trufModel.setId(jsonObject1.getInt("id"));
                                trufModel.setCid(jsonObject1.getInt("cid"));
                                trufModel.setScid(jsonObject1.getInt("scid"));
                                trufModel.setVid(jsonObject1.getInt("vid"));
                                trufModel.setTname(jsonObject1.getString("tname"));
                                trufModel.setAddress(jsonObject1.getString("address"));
                                trufModel.setDescription(jsonObject1.getString("descr"));
                                trufModel.setProcingset(jsonObject1.getString("pricing_set"));
                                trufModel.setPrice(jsonObject1.getString("turf_price"));
                                trufModel.setDay(jsonObject1.getString("day"));
                                trufModel.setFromtime(jsonObject1.getString("from_time"));
                                trufModel.setTotime(jsonObject1.getString("to_time"));
                                trufModel.setImages(jsonObject1.getString("images"));
                                trufModel.setTrufID(jsonObject1.getInt("turf_id"));
                                trufModel.setRating(jsonObject1.getInt("rate"));
                                trufModelArrayList.add(trufModel);

                            }
                            if (trufModelArrayList.size() != 0) {
                                searchAdapter = new SearchAdapter(SearchActivity.this, "Fragment", trufModelArrayList, "");
                                rvSearch.setAdapter(searchAdapter);

                                setClickEventOfListView();
                            } else {
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }
                        } else {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    tvNoDataFound.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private void setClickEventOfListView() {
        searchAdapter.setOnItemClickListener(new SearchAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent i = new Intent(SearchActivity.this, SportsDetailActivity.class);
                i.putExtra("trufdata", trufModelArrayList.get(position));
                i.putExtra("date", Date);
                startActivity(i);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });
    }

}