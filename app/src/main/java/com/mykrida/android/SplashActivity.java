package com.mykrida.android;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.firebase.analytics.FirebaseAnalytics;

public class SplashActivity extends Activity {

    SharedPreferences prefUserData;
    VideoView videoView;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);

//        videoView = findViewById(R.id.videoView);
//        Uri uri = Uri.parse("android.resource://com.mykrida.android/"+R.raw.intro_2);
//        MediaController mc = new MediaController(this);
//        videoView.setMediaController(mc);
//        videoView.setVideoURI(uri);
//        videoView.start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(prefUserData.getBoolean("isLoggedIn",false)){
                    Intent i = new Intent(SplashActivity.this,DashboardActivity.class);
                    startActivity(i);
                    overridePendingTransition(0,0);
                    finish();
                }else {
                    Intent i = new Intent(SplashActivity.this, IntroActivity.class);
                    startActivity(i);
                    overridePendingTransition(0, 0);
                    finish();
                }
            }
        },1000);

    }
}