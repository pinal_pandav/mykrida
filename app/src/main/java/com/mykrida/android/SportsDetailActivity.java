package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.ViewPagerAdapter;
import com.mykrida.android.Fragment.LocationFragment;
import com.mykrida.android.Fragment.OverviewFragment;
import com.mykrida.android.Fragment.ReviewFragment;
import com.mykrida.android.Model.TrufModel;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONException;
import org.json.JSONObject;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SportsDetailActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    OverviewFragment overviewFragment;
    ReviewFragment reviewFragment;
    LocationFragment locationFragment;
    ImageView btnBack;
    public TrufModel trufModel;
    TextView tvTrufName;
    SmoothBottomBar smoothBottomBar;
    public View llReview;
    CardView btnCancelReview,btnSendReview;
    ApiInterface apiService;
    KProgressHUD hud;
    SharedPreferences prefUserData;
    JsonObject jobjUser;
    ScaleRatingBar scaleRatingBar;
    EditText edtReview;
    String Date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_details);
        getSupportActionBar().hide();

        tvTrufName = findViewById(R.id.tvTrufName);
        llReview = findViewById(R.id.llReview);
        scaleRatingBar =findViewById(R.id.simpleRatingBar);
        edtReview = findViewById(R.id.edtReview);
        btnCancelReview = findViewById(R.id.btnCancel);
        btnSendReview = findViewById(R.id.btnReview);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        prefUserData = getSharedPreferences("USERDATA",MODE_PRIVATE);
        String UserInfo = prefUserData.getString("User_Info","");
        jobjUser = new Gson().fromJson(UserInfo, JsonObject.class);


        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position,false);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        trufModel = getIntent().getExtras().getParcelable("trufdata");
        Date = getIntent().getStringExtra("date");
        tvTrufName.setText(trufModel.getTname());

        setupViewPager(viewPager);


        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(SportsDetailActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(SportsDetailActivity.this,DashboardActivity.class);
                intent.putExtra("position",i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        btnCancelReview.setOnClickListener(view -> {
            llReview.setVisibility(View.GONE);
        });

        btnSendReview.setOnClickListener(view -> {
            AddReview();
        });

        llReview.setOnClickListener(view -> {
            llReview.setVisibility(View.GONE);
        });

    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        overviewFragment=new OverviewFragment().getInstant(trufModel.getTrufID(),Date);
        reviewFragment=new ReviewFragment();
        locationFragment = new LocationFragment().getInstant(trufModel.getTrufID(),Date);
        adapter.addFragment(overviewFragment,"Overview");
        adapter.addFragment(reviewFragment,"Review");
        adapter.addFragment(locationFragment,"Location");
        viewPager.setAdapter(adapter);
    }

    private void AddReview() {
        hud.show();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.AddReview(Constant.APPKEY,jobjUser.get("id").getAsInt(),trufModel.getTrufID(),edtReview.getText().toString().trim());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            AddRating();
                        } else {
                            Toast.makeText(SportsDetailActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }else{
                    hud.dismiss();
                    llReview.setVisibility(View.GONE);
                    edtReview.setText("");
                    scaleRatingBar.setRating(0);
                    Toast.makeText(SportsDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void AddRating() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.AddRate(Constant.APPKEY,jobjUser.get("id").getAsInt(),trufModel.getTrufID(),scaleRatingBar.getRating());
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getString("status").equals("1")) {
                            llReview.setVisibility(View.GONE);
                            edtReview.setText("");
                            scaleRatingBar.setRating(0);
                        } else {
                            Toast.makeText(SportsDetailActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }
                }else{
                    hud.dismiss();
                    llReview.setVisibility(View.GONE);
                    edtReview.setText("");
                    scaleRatingBar.setRating(0);
                    Toast.makeText(SportsDetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }
}