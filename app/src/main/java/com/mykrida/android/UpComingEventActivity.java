package com.mykrida.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.mykrida.android.API.ApiClient;
import com.mykrida.android.API.ApiInterface;
import com.mykrida.android.API.Constant;
import com.mykrida.android.Adapter.UpcomingEventsAdapter;
import com.mykrida.android.Model.EventModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.ibrahimsn.lib.OnItemReselectedListener;
import me.ibrahimsn.lib.OnItemSelectedListener;
import me.ibrahimsn.lib.SmoothBottomBar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpComingEventActivity extends AppCompatActivity {

    RecyclerView rvUpcomingEvent;
    UpcomingEventsAdapter upcomingEventsAdapter;
    ArrayList<EventModel> arrEvents = new ArrayList<>();
    TextView tvNoDataFound;

    SmoothBottomBar smoothBottomBar;
    ApiInterface apiService;
    KProgressHUD hud;
    ImageView btnBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_upcoming_events);

        getSupportActionBar().hide();

        btnBack = findViewById(R.id.btnBack);

        btnBack.setOnClickListener(view -> {
            onBackPressed();
        });

        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        rvUpcomingEvent = findViewById(R.id.rvUpcomingEvents);
        tvNoDataFound = findViewById(R.id.tvNoDataFound);

        rvUpcomingEvent.setHasFixedSize(true);
        rvUpcomingEvent.setLayoutFrozen(true);
        LinearLayoutManager llm = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvUpcomingEvent.setLayoutManager(llm);

        smoothBottomBar = findViewById(R.id.bottomBar);
        smoothBottomBar.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public boolean onItemSelect(int i) {
                Intent intent = new Intent(UpComingEventActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return false;
            }
        });

        smoothBottomBar.setOnItemReselectedListener(new OnItemReselectedListener() {
            @Override
            public void onItemReselect(int i) {
                Intent intent = new Intent(UpComingEventActivity.this, DashboardActivity.class);
                intent.putExtra("position", i);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        GetEventList();

    }

    private void GetEventList() {
        apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<String> callCard = apiService.GetEventList(Constant.APPKEY);
        callCard.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                hud.dismiss();
                if (response.code() == 200) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        arrEvents.clear();
                        if (jsonObject.getString("status").equals("1")) {
                            for (int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
                                JSONObject jsonObject1 = jsonObject.getJSONArray("data").getJSONObject(i);
                                EventModel eventModel = new EventModel();
                                eventModel.setDate(jsonObject1.getString("date"));
                                eventModel.setDescription(jsonObject1.getString("description"));
                                eventModel.setId(jsonObject1.getInt("id"));
                                eventModel.setImage(jsonObject1.getString("images"));
                                eventModel.setPlace(jsonObject1.getString("place"));
                                eventModel.setPrice(jsonObject1.getInt("ticket_price"));
                                eventModel.setTitle(jsonObject1.getString("title"));
                                eventModel.setType(jsonObject1.getInt("type"));

                                arrEvents.add(eventModel);
                            }

                            if(arrEvents.size() != 0) {
                                upcomingEventsAdapter = new UpcomingEventsAdapter(UpComingEventActivity.this,"",arrEvents,"");
                                rvUpcomingEvent.setAdapter(upcomingEventsAdapter);


                                setAdapterClick();
                            }else{
                                tvNoDataFound.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(UpComingEventActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        hud.dismiss();
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                hud.dismiss();
            }
        });
    }

    private void setAdapterClick() {

        upcomingEventsAdapter.setOnItemClickListener(new UpcomingEventsAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Intent intent = new Intent(UpComingEventActivity.this, EventDetailsActivity.class);
                intent.putExtra("eventdetails",arrEvents.get(position));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(int position, View v) {

            }
        });


    }

}