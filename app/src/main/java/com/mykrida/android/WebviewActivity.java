package com.mykrida.android;

import android.app.Activity;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.mykrida.android.databinding.ActivityAboutUsBinding;
import com.mykrida.android.databinding.ActivityWebviewBinding;

public class WebviewActivity extends Activity {

    ActivityWebviewBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview);

        binding.btnBack.setOnClickListener(view -> onBackPressed());

        binding.webview.loadUrl(getIntent().getStringExtra("url"));
        binding.tvTitle.setText(getIntent().getStringExtra("title"));
    }
}